import React from "react";
import {MainContent, MainContentBody, MainContentHeader} from '../components/content.main';
import {CartModal} from "../cart/CartModal";
import { connect } from "react-redux";
import {restaurantSlice} from '../reducer';
import {RestaurantItem} from "../components/RestaurantItem";
import '../../css/restaurant.css';
import {COVER_IMAGE, LOGO} from '../../images';
import {fetchRestaurantsList} from "../reducer";

class RestaurantsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurants: [],
            user: null,
            foodParties: [{
                restaurant:{name: "khames"},
                newPrice: 10000,
                count: 5,
                food:{price: 20000, name: "kabab"}
            }]
        }
    }

    componentDidMount() {
        this.props.fetchRestaurantsList();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.restaurants)
            this.setState({restaurants: nextProps.restaurants});
    }

    render() {
        return (
            <MainContent>
                <MainContentHeader>
                    <img src={COVER_IMAGE} className="top"/>
                    <div className="top">
                        <a href="#">
                            <img src={LOGO} className="logo"/>
                        </a>
                        <div className="description">
                            اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران
                        </div>
                    </div>
                    {/*<TabNavigation/>*/}
                </MainContentHeader>
                <MainContentBody>
                    <div className="container">
                        <div className="title-style-1">جشنواره غذا</div>
                        <div className="row">
                            {
                                // this.state.foodParties?this.state.foodParties.map((value, index) => {
                                //     return <FoodPartyItem FoodPartyItem={this.state.foodParties[index]}/>
                                // }):""
                            }
                        </div>
                    </div>
                    <div className="container">
                        <div className="title-style-1">رستوران ها</div>
                        <div className="row">
                            {
                                this.state.restaurants?this.state.restaurants.map((value, index) => {
                                    return <RestaurantItem name={value.name} id={value.id}/>
                                }):""
                            }
                        </div>
                    </div>
                </MainContentBody>
            </MainContent>
        );
    }
}
const mapStateToProps = state => {
    return {
        restaurants: state.restaurant.restaurants
    };
};



export default RestaurantsPage = connect(mapStateToProps,{
    fetchRestaurantsList
})(RestaurantsPage);
