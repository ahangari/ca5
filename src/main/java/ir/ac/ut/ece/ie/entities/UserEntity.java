package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserEntity extends BaseEntity<String>{
    public static final String COL_ID = "id";
    public static final String COL_FIRSTNAME = "firstname";
    public static final String COL_LASTNAME = "lastname";
    public static final String COL_PHONENUMBER = "phonenumber";
    public static final String COL_EMAIL = "email";
    public static final String COL_CREDIT = "credit";
    public static final String COL_LOCATION = "Location";
    public static final String COL_PASSWORD = "password";
    public static final String COL_USERNAME = "username";

    private String id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private BigDecimal credit;
    private String password;
    private String userName;
    @JsonIgnore
    private Location location = new Location();


    public UserEntity() {

    }

    public UserEntity(String id){
        this.id = id;
    }

    public BigDecimal decreaseCredit(int amount) {
        credit = new BigDecimal(credit.intValue()- amount);
        return credit;
    }
}
