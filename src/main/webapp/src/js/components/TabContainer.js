import React from 'react';

export default function TabContainer(props) {
    let clazzName = "tab-pane fade generic-tab-content-style";
    if (props.active == true)
        clazzName = clazzName + " active";
    if (props.show == true)
        clazzName = clazzName + " show";

    return (
        <div className={clazzName} id={"pills-" + props.name} role="tabpanel"
             aria-labelledby={"pills-" + props.name + "-tab"}>
            {props.children}
        </div>
    );
}