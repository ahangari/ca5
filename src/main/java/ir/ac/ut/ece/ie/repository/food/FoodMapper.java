package ir.ac.ut.ece.ie.repository.food;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.FoodEntity.*;

public class FoodMapper extends Mapper<FoodEntity, String> {

    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        columns.put(COL_NAME, String.class);
        columns.put(COL_DESCRIPTION, String.class);
        columns.put(COL_IMAGE, String.class);
        columns.put(COL_POPULARITY, BigDecimal.class);
        columns.put(COL_PRICE, BigDecimal.class);
        columns.put(COL_MENU, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, FoodEntity foodEntity) {
        switch (column){
            case COL_ID:
                return foodEntity.getId();
            case COL_NAME:
                return foodEntity.getName();
            case COL_DESCRIPTION:
                return foodEntity.getDescription();
            case COL_IMAGE:
                return foodEntity.getImage();
            case COL_POPULARITY:
                return foodEntity.getPopularity();
            case COL_PRICE:
                return foodEntity.getPrice();
            case COL_MENU:
                return foodEntity.getMenu().getId();
            default:
                return null;
        }
    }

    @Override
    protected FoodEntity convertResultSetToObject(ResultSet rs) throws SQLException {
        FoodEntity food = new FoodEntity();
        food.setId(rs.getString(COL_ID));
        food.setName(rs.getString(COL_NAME));
        food.setImage(rs.getString(COL_IMAGE));
        food.setDescription(rs.getString(COL_DESCRIPTION));
        food.setPopularity(rs.getDouble(COL_POPULARITY));
        food.setPrice(rs.getBigDecimal(COL_PRICE));
        food.setMenu(new MenuEntity(rs.getString(COL_MENU), null));
        return food;
    }

    @Override
    public String getDefaultTable() {
        return Constants.FOOD_TABLE;
    }
}
