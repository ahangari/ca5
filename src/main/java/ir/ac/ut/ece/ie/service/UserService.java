package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.Exception.LocalizedException;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.manager.LoginResponseDto;
import ir.ac.ut.ece.ie.manager.OrderManager;
import ir.ac.ut.ece.ie.manager.UserManager;
import ir.ac.ut.ece.ie.service.dto.CreditDto;
import ir.ac.ut.ece.ie.service.dto.OrderDto;
import ir.ac.ut.ece.ie.service.dto.OrdersDto;
import ir.ac.ut.ece.ie.service.util.ActionResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserService {
    public static final String USER_TEST = "user1";

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UserEntity getUser(@PathVariable("id")String id) {
        UserEntity user = UserManager.getInstance().load(id);
        return user;
    }
    @PostMapping(value = "/increseCredit")
    public ActionResult increaseCredit(@RequestBody CreditDto credit) {
        UserEntity user = UserManager.getInstance().load(USER_TEST);
        UserManager.getInstance().increaseCredit(user, Integer.parseInt(credit.getCredit()));
        return new ActionResult("successfully increase!");
    }
    @GetMapping(value = "/getOrders/{id}")
    public OrdersDto getOrders(@PathVariable("id")String userId){
        OrdersDto result = new OrdersDto();
        List<OrderDto> orderDtos = new ArrayList<>();
        List<OrderEntity> orders = UserManager.getInstance().getAllOrders(userId);
        orders.stream().
                forEach(order ->{
                        OrderDto dto = new OrderDto();
                        dto.setItems(OrderManager.getInstance().loadOrderItems(order.getId()));
                        dto.setStatus(order.getStatus());
                        dto.setRestaurant(OrderManager.getInstance().getRestaurantOfOrder(order));
                        orderDtos.add(dto);
                        });
        result.setOrders(orderDtos);
        return result;
    }

    @PostMapping(value = "/signup")
    public ActionResult registerUser(@RequestBody UserEntity user){
        UserManager.getInstance().resgisterUser(user);
        return new ActionResult("successfully added!");
    }

    @PostMapping(value = "/login")
    public LoginResponseDto loginUser(@RequestBody UserEntity user){
        try {
            return UserManager.getInstance().loginUser(user);
        } catch (LocalizedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
