import React from 'react';
import {Link} from 'react-router-dom';

import {CART_MODAL_ID} from "../Constants";
import {BtnOpenModal} from "../components/modal";
import {LOGO} from "../../images";


export default class NavigationBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            this.props.authed?
            <nav className="navbar navbar-expand-lg fixed-top navbar-light" style={{backgroundColor: "#FFFFFF"}}>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <Link className="navbar-brand" to="/dashboard">
                    <img src={LOGO} style={{height: "40px", width: "40px"}}/>
                </Link>
                <div className="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item active">
                            <BtnOpenModal modalId={CART_MODAL_ID} clazzName={"btn flaticon-smart-cart"} onItemClick={()=>{}}>
                            </BtnOpenModal>
                        </li>
                        <li className="nav-item">
                            <Link to="/profile">
                                <div >حساب کاربری</div>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/logout" className="nav-link">خروج</Link>
                        </li>
                    </ul>
                </div>
            </nav>
                :<div></div>
        );
    }
}
