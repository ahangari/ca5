import React from 'react';
import Food from "./FoodItem";
import MenuItemModal from "./FoodItemModal";

const MENU = "منوی غذا";

export default class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            itemSelected: {}
        }
    }
    onItemClick(item){
        this.setState({itemSelected: item});
    }
    render() {
        return (
            <div>
                <div style={{marginBottom: "3%"}}>
                    {MENU}
                </div>
                <div className="row">
                    {
                        this.props.foods?this.props.foods.map((value, index) => {
                            return <Food
                                item={value}
                                onItemClick={this.onItemClick.bind(this)}
                            />
                        }):""
                    }
                </div>
                <MenuItemModal restaurantName={this.props.restaurantName} food={this.state.itemSelected}/>
            </div>
        );
    }

}
