package ir.ac.ut.ece.ie.repository.mapper;

import java.sql.SQLException;

public interface IMapper<T, I> {
    T load(I id) throws SQLException;
    void save(T t) throws SQLException;
    void delete(I id) throws SQLException;
}
