import React from 'react';
import {connect} from 'react-redux';
import {cartSlice} from "../reducer";

const {changeFoodCount} = cartSlice.actions;

class CartFoodItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: this.props.count
        }
    }

    increaseCount() {
        let count = this.state.count + 1;
        this.setState({count: count});
        this.props.changeFoodCount(this.props.food, count);
    }

    decreaseCount() {
        if (this.state.count != 0) {
            let count = this.state.count - 1;
            this.setState({count: count});
            this.props.changeFoodCount(this.props.food, count);
        }
    }

    calculateTotalPrice() {
        return this.props.food.price * this.state.count;
    }

    render() {
        return (
            <div className="cart-food-item">
                <div className="row">
                    <div className="col-8" style={{textAlign: "start"}}>{this.props.food.name}</div>
                    <div className="col-4" style={{padding: 0}}>
                        <a className="flaticon-plus" style={{color: "green", textAlign: "center"}}
                           onClick={() => this.increaseCount()}/>
                        <span className="cart-food-counter-text">{this.state.count}</span>
                        <a className="flaticon-minus" style={{color: "red"}} onClick={() => this.decreaseCount()}/>
                    </div>
                </div>
                <div style={{textAlign: "end"}}>
                    {this.calculateTotalPrice()} تومان
                </div>
            </div>
        );
    }
}

export default connect(null, {changeFoodCount})(CartFoodItem);