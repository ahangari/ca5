import React from 'react';

import {createSlice} from "@reduxjs/toolkit";

const authState = {
    jwtToken: null,
    redirect: false,
    error: "",
    user: {
        id: "user1"
    }
};


export const authSlice = createSlice({
    name: "auth",
    initialState: authState,
    reducers: {
        loginSuccess: (state, action) => {
            state.jwtToken = action.payload.jwtToken;
            state.user = action.payload.user;
            state.redirect = true;
        },
        loginFailed: (state, action) => {
            state.error = action.payload;
        },
        logout: (state, action) => {
            state.jwtToken = null;
            state.user = null
            state.redirect = false;
        }
    },
});

const restaurantState = {
    restaurants: []
};

export const restaurantSlice = createSlice({
    name: "restaurant",
    initialState: restaurantState,
    reducers: {
        addRestaurants: (state, action) => {
            state.restaurants = action.payload;
        }
    }
});

const cartState = {
    user: null,
    items: [],
    result: {
        message: null,
        status: null
    }
};

export function loginUser(user) {
    return function (dispatch) {
        const requestOptions = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(user),
            headers: {
                "Content-Type": "application/json"
            }
        };
        return fetch(serverPath + 'login', requestOptions)
            .then(resp => resp.json())
            .then(data => {
                dispatch(authSlice.actions.loginSuccess(data));
            }, error => {
                dispatch(authSlice.actions.loginFailed(error));
            })
    }
}

export function fetchRestaurantsList() {
    return function (dispatch, getState) {
        console.log(getState().auth.jwtToken);
        const myHeaders = new Headers();
        myHeaders.append('Authorization', 'Bearer ' + getState().auth.jwtToken);
        // myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
        // myHeaders.append('Accept', 'application/json');
        const requestOptions = {
            method: 'GET',
            mode: 'cors',
            credentials: 'same-origin',
            headers: myHeaders
        };
        return fetch(serverPath + 'getRestaurants', requestOptions)
            .then(resp => resp.json())
            .then(data => {
                dispatch(restaurantSlice.actions.addRestaurants(data.restaurants));
            })
            ;
    }
}

export function signupUser(user) {
    return function (dispatch) {
        const requestOptions = {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(user),
            headers: {
                "Content-Type": "application/json"
            }
        };
        return fetch(serverPath + 'signup', requestOptions)
            .then(resp => resp.json())
            .then(data => {
                // dispatch(authSlice.actions.loginSuccess(data));
                alert(data)
            }, error => {
                // dispatch(authSlice.actions.loginFailed(error));
                alert(error)
            })
    }
}


export function finalizeCart(items, user) {
    return function (dispatch, getState) {
        const requestOptions = {
            method: 'PUT',
            mode: 'cors',
            body: JSON.stringify({items: items, user: user}),
            headers: {
                "Content-Type": "application/json",
                Authorization: 'Bearer ' + getState().auth.jwtToken
            }
        };
        return fetch(serverPath + 'cart', requestOptions)
            .then(resp => resp.json())
            .then(data => {
                dispatch(cartSlice.actions.finalizeCart(data));
            })
    }
}

export const cartSlice = createSlice({
    name: "cart",
    initialState: cartState,
    reducers: {
        addItem: (state, action) => {
            state.items.push(action.payload);
        },
        removeItem: (state, action) => {
        },
        changeFoodCount: (state, action) => {
            let food = action.payload.food;
            let count = action.payload.count;
            state.items.map((value, index) => {
                if (value.food == food) {
                    state.items = {...state.items[index].count = count};
                    return;
                }
            })
        },
        finalizeCart: (state, action) => {
            state.result = action.payload;
        }
    }
});