package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.food.FoodRepository;

import java.util.List;

public class FoodManager extends BaseManager<FoodEntity> {

    private FoodManager() {
        super.loadAll();
    }

    private static FoodManager instance = new FoodManager();

    public static FoodManager getInstance() {
        return instance;
    }

    public List<FoodEntity> getFoodsByMenu(String menuId){
        return FoodRepository.getInstance().getFoodsByMenu(menuId);
    }

    public FoodEntity loadByName(String name){
        return FoodRepository.getInstance().loadByName(name);
    }

//    public void addFood(FoodEntity food) {
//        RestaurantManager restaurantManager = RestaurantManager.getInstance();
//        if (restaurantManager.restaurantExists(this.getRestaurant(food).getName())) {
//            if (!(this.getRestaurant(food).restaurantHasFood(food.getName()))) {
//                restaurantManager.addFood(food);
//            }
//            foods.put(food.getId(), food);
//        } else
//            System.out.println("no Restaurant matched");
//    }

    public RestaurantEntity getRestaurantOfFood(FoodEntity food) {
        MenuEntity menuEntity = MenuManager.getInstance().load(food.getMenu().getId());
        return MenuManager.getInstance().getRestaurant(menuEntity);
    }

    @Override
    protected BaseRepository getRepository() {
        return FoodRepository.getInstance();
    }
}
