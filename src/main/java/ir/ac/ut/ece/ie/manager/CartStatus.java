package ir.ac.ut.ece.ie.manager;

public enum CartStatus {
    LONG_DISTANCE,
    NOT_EXIST,
    NOT_ENOUGH_MONEY,
    EMPTY_CART,
    OK;
}
