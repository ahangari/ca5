package ir.ac.ut.ece.ie.repository.mapper;

import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.repository.TestConnectionPool;
import ir.ac.ut.ece.ie.repository.menu.MenuMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class MenuMapperTest {
    private MenuEntity test;
    private MenuMapper mapper;
    private Connection connection;

    private static final String ID = "menu1";
    @Before
    public void setUp() throws Exception {
        mapper = new MenuMapper();
        test = new MenuEntity(ID, null);
        connection = TestConnectionPool.getConnection();
        mapper.save(test);
    }

    @Test
    public void testSaveEntity() throws SQLException {
        MenuEntity actual = mapper.load(ID);
        MenuEntity expected = new MenuEntity(ID, null);
        assertEquals(expected.getId(), actual.getId());
    }

    @After
    public void tearDown() throws Exception {
        mapper.delete(ID);
        connection.close();
    }

}