import React from 'react';

export default class ProfileDetail extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="row">
                <div className="col-8">
                    <div className="row" style={{fontSize: "50px", marginRight: "20px"}}>
                        <span style={{fontSize: "50px"}} className="flaticon-account"></span>
                        <span>{this.props.fullName}</span>
                    </div>
                </div>
                <div className="col-4">
                    <div className="justify-content-center">
                        <div className="row">
                            <span className="flaticon-phone"></span>
                            <span> {this.props.phoneNumber}</span>
                        </div>
                        <div className="row">
                            <span className="flaticon-mail"></span>
                            <span> {this.props.email}</span>
                        </div>
                        <div className="row">
                            <span className="flaticon-card"></span>
                            <span>{this.props.credit} تومان</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
