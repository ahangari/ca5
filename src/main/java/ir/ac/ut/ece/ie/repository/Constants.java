package ir.ac.ut.ece.ie.repository;

public class Constants {
    public static final String RESTAURANT_TABLE = "restaurant";
    public static final String MENU_TABLE = "menu";
    public static final String FOOD_TABLE = "food";
    public static final String ORDER_TABLE = "_order";
    public static final String ORDER_HAS_FOOD_TABLE = "order_has_food";
    public static final String USER_TABLE = "user";
    public static final String FOODPARTY_MENU_TABLE = "foodparty_menu";
    public static final String FOODPARTY_HAS_FOOD_TABLE = "foodparty_has_food";


}
