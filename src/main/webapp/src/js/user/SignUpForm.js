import React from 'react';
import {connect} from 'react-redux';
import {signupUser} from "../reducer";
import '../../css/login_styles.css';

class SignUpForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                firstName: null,
                lastName: null,
                phoneNumber: null,
                email: null,
                userName: null,
                password: null,
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const value = event.target.value;
        switch (event.target.name) {
            case "firstName":
                this.setState({user: {...this.state.user, firstName: value}});
                break;
            case "lastName":
                this.setState({user: {...this.state.user, lastName: value}});
                break;
            case "phoneNumber":
                this.setState({user: {...this.state.user, phoneNumber: value}});
                break;
            case "email":
                this.setState({user: {...this.state.user, email: value}});
                break;
            case "userName":
                this.setState({user: {...this.state.user, userName: value}});
                break;
            case "password":
                this.setState({user: {...this.state.user, password: value}});
                break;
        }
    }

    handleSubmit(event){
        event.preventDefault();
        console.log(this.state.user);
        this.props.signupUser(this.state.user);
    }

    render() {
        return (
            <div className="form_container">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <h2>صفحه ثبت نام</h2>
                    </div>
                    <div className="container">
                        <label className="credential_labels"><b>نام</b></label>
                        <input type="text" value={this.state.firstName} onChange={this.handleChange}
                               placeholder="نام خود را وارد کنید" name="firstName" required/>
                        <b></b>
                        <label className="credential_labels"><b>نام خانوادگی</b></label>
                        <input type="text" value={this.state.lastName} onChange={this.handleChange}
                               placeholder="نام خانوادگی خود را وارد کنید" name="lastName" required/>
                        <b></b>
                        <label className="credential_labels"><b>شماره تلفن</b></label>
                        <input type="text" value={this.state.phoneNumber} onChange={this.handleChange}
                               placeholder="شماره تلفن خود را وارد کنید" name="phoneNumber" required/>
                        <b></b>
                        <label className="credential_labels"><b>آدرس ایمیل</b></label>
                        <input type="text" value={this.state.email} onChange={this.handleChange}
                               placeholder="آدرس ایمیل خود را وارد کنید" name="email" required/>
                        <b></b>
                        <label className="credential_labels"><b>نام کاربری</b></label>
                        <input type="text" value={this.state.userName} onChange={this.handleChange}
                               placeholder="نام کاربری خود را وارد کنید" name="userName"
                               required/>
                        <b></b>
                        <label className="credential_labels"><b>رمز عبور</b></label>
                        <input type="password" value={this.state.password} onChange={this.handleChange}
                               placeholder="رمز عبور خود را وارد کنید" name="password"
                               required/>
                        <b></b>
                        {/*<label className="credential_labels"><b>تایید رمز عبور</b></label>*/}
                        {/*<input type="text" placeholder="رمز عبور خود را تایید کنید" name="password"*/}
                        {/*       required/>*/}
                        {/*<b></b>*/}
                        <button type="submit" className="submit-btn-style">ثبت نام
                        </button>
                    </div>
                    <div className="container" style={{backgroundColor: "#f1f1f1"}}>
                        <button type="button" className="down_button">ثبت نام با استفاده از Google Authentication
                        </button>
                    </div>
                    <div className="container" style={{backgroundColor: "#f1f1f1"}}>
                        <button type="button" className="down_button">قبلا عضو شده بودم</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default connect(null, {
    signupUser
})(SignUpForm);