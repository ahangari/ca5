package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class FoodEntity extends BaseEntity<String> {
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_PRICE = "price";
    public static final String COL_POPULARITY = "popularity";
    public static final String COL_IMAGE = "image";
    public static final String COL_MENU = "menu_id";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    private String name;
    private String description;
    private BigDecimal price;
    private Double popularity;
    private String image;
    @JsonIgnore
    private MenuEntity menu;

    public FoodEntity() {
    }

    public FoodEntity(String id){
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FoodEntity)) return false;
        FoodEntity that = (FoodEntity) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getPrice(), that.getPrice()) &&
                Objects.equals(getPopularity(), that.getPopularity()) &&
                Objects.equals(getImage(), that.getImage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getPrice(), getPopularity(), getImage());
    }
}
