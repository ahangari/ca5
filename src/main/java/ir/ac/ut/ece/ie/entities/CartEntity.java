package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.ac.ut.ece.ie.entities.model.OrderItem;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CartEntity {

    private List<OrderItem> items = new ArrayList<>();
    private UserEntity user;

    @JsonIgnore
    private RestaurantEntity currentRestaurant;
    @JsonIgnore
    private DeliveryEntity deliveryGuy;


    public boolean addItem(OrderItem item) {
        items.add(item);
        return true;
    }

    public void emptyCart() {
        items.clear();
    }

//    public void setCurrentRestaurant(RestaurantEntity restaurant) {
//        if(this.currentRestaurant != null)
//            return;
//        this.currentRestaurant = restaurant;
//    }

    public void assignDelivery(DeliveryEntity delivery) {
        this.setDeliveryGuy(delivery);
//        delivery.setStatus(DeliveryStatus.BUSY);
    }
}
