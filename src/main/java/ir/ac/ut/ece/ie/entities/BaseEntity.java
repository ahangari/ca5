package ir.ac.ut.ece.ie.entities;

public abstract class BaseEntity<I> {
    public abstract I getId();
    public abstract void setId(I id);
}
