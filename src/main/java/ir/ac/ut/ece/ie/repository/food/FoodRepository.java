package ir.ac.ut.ece.ie.repository.food;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.menu.MenuRepository;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.sql.SQLException;
import java.util.List;

public class FoodRepository extends BaseRepository<FoodEntity> {
    private static FoodRepository instance = new FoodRepository();

    public static FoodRepository getInstance() {
        return instance;
    }

    private FoodRepository(){
    }
    public MenuEntity getMenuOfFood(String id) throws SQLException {
        FoodMapper foodMapper = new FoodMapper();
        FoodEntity food = foodMapper.load(id);
        MenuEntity menu = MenuRepository.getInstance().getMenuById(food.getMenu().getId());
        return menu;
    }

    public List<FoodEntity> getFoodsByMenu(String menuId){
        FoodMapper foodMapper = new FoodMapper();
        return foodMapper.getEntitiesByProp(FoodEntity.COL_MENU, menuId);
    }

    @Override
    protected Mapper<FoodEntity, String> getEntityMapper() {
        return new FoodMapper();
    }

    public FoodEntity loadByName(String name) {
        FoodMapper foodMapper = new FoodMapper();
        List<FoodEntity> result = foodMapper.getEntitiesByProp(FoodEntity.COL_NAME, name);
        if(result.size()==0){
            return null;
        }
        return result.get(0);
    }
}
