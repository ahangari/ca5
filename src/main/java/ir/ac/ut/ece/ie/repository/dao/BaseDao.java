package ir.ac.ut.ece.ie.repository.dao;

import java.util.List;
import java.util.Map;

public abstract class BaseDao {
    public abstract Map<String, Class<?>> getColumns();
    public abstract String getAssociationTable();
    public abstract Object getColumnValue(String column);
    public abstract List<String> getPk();
}
