import React from 'react';
import '../../css/login_styles.css'
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {authSlice} from "../reducer";

const {logout} = authSlice.actions;

class Logout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        this.props.logout();
        alert(logout)
        return (
            <Redirect to={{pathname: '/login', state: {from: this.props.location}}}/>
        );
    }
}

export default connect(null, {
    logout
})(Logout);