import React from 'react';
import CartCore from "./CartCore";
class CartInPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: props.cart
        }
    }

    render() {
        return (
            <div>
                <div style={{color: "#F7FFF7"}}>
                    صورت حساب
                </div>
                <div className="shadow-box">
                    <div style={{textAlign: "center", marginBottom: "20px"}}>
                        <span className="cart-title">
                            {"سبد خرید"}
                        </span>
                        <CartCore />
                    </div>
                </div>
            </div>
        );
    }
}

export default CartInPage;