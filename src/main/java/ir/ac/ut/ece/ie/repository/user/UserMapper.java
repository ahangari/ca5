package ir.ac.ut.ece.ie.repository.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.UserEntity.*;

public class UserMapper extends Mapper<UserEntity, String> {

    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        columns.put(COL_FIRSTNAME, String.class);
        columns.put(COL_LASTNAME, String.class);
        columns.put(COL_EMAIL, String.class);
        columns.put(COL_PHONENUMBER, String.class);
        columns.put(COL_CREDIT, BigDecimal.class);
        columns.put(COL_LOCATION, String.class);
        columns.put(COL_PASSWORD, String.class);
        columns.put(COL_USERNAME, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, UserEntity userEntity) {
        switch (column){
            case COL_ID:
                return userEntity.getId();
            case COL_FIRSTNAME:
                return userEntity.getFirstName();
            case COL_LASTNAME:
                return userEntity.getLastName();
            case COL_EMAIL:
                return userEntity.getEmail();
            case COL_PHONENUMBER:
                return userEntity.getPhoneNumber();
            case COL_CREDIT:
                return userEntity.getCredit();
            case COL_LOCATION:
                return userEntity.getLocation().toString();
            case COL_PASSWORD:
                return userEntity.getPassword();
            case COL_USERNAME:
                return userEntity.getUserName();
            default:
                return null;
        }
    }

    @Override
    protected UserEntity convertResultSetToObject(ResultSet rs) throws SQLException {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(rs.getString(COL_ID));
        userEntity.setFirstName(rs.getString(COL_FIRSTNAME));
        userEntity.setLastName(rs.getString(COL_LASTNAME));
        userEntity.setPhoneNumber(rs.getString(COL_PHONENUMBER));
        userEntity.setEmail(rs.getString(COL_EMAIL));
        userEntity.setCredit(rs.getBigDecimal(COL_CREDIT));
        userEntity.setUserName(rs.getString(COL_USERNAME));
        userEntity.setPassword(rs.getString(COL_PASSWORD));
        userEntity.setUserName(rs.getString(COL_USERNAME));
        try {
            userEntity.setLocation(new ObjectMapper().readValue(rs.getString(UserEntity.COL_LOCATION), Location.class));
        } catch (IOException e) {
            e.printStackTrace();
            userEntity.setLocation(null);
        }
        return userEntity;
    }

    @Override
    public String getDefaultTable() {
        return Constants.USER_TABLE;
    }
}
