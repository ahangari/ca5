import React from 'react';
import {ModalComponent, ModalBody, ModalHeader} from "../components/modal";
import CartCore from "./CartCore";
import {CART_MODAL_ID} from "../Constants";

export class CartModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: this.props.cart
        }
    }

    render() {
        return (
            this.props.authed?
            <ModalComponent size="small" id={CART_MODAL_ID}>
                <ModalHeader title="سبد خرید" id={CART_MODAL_ID}/>
                <ModalBody>
                    <CartCore />
                </ModalBody>
            </ModalComponent>
                :
                null
        );
    }
}






