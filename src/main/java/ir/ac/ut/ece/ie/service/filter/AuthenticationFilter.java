package ir.ac.ut.ece.ie.service.filter;


import ir.ac.ut.ece.ie.manager.jwt.JwtTokenUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest _request, ServletResponse _response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) _request;
        HttpServletResponse response = (HttpServletResponse) _response;
        String path = request.getRequestURI();
        String authorization = request.getHeader("Authorization");

        if (path.contains("getRestaurants") || path.contains("/getMenu") || path.contains("/cart") ||
                path.contains("/foodparty") || path.contains("/getOrders") || path.contains("/increseCredit") ||
                path.contains("/user")) {
            if (authorization != null) {
                JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
                if (jwtTokenUtil.validateToken(authorization))
                    chain.doFilter(_request, _response);
                else {
                    response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
                    chain.doFilter(_request, response);
                }
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You Should login first to see this content");
//                chain.doFilter(_request, response);
                return;
            }
        } else {
            chain.doFilter(_request, _response);
        }
    }

    @Override
    public void destroy() {

    }
}
