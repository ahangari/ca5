import React from 'react';
import {MainContent, MainContentBody, MainContentHeader} from "../components/content.main";
import CartInPage from "../cart/CartPage";
import '../../css/restaurant.css';
import Menu from "./Menu";
import {MC_IMAGE} from "../../images";

function RestaurantMenuTitle(props) {
    return (
        <div className="restaurant-logo">
            <img className="border restaurant-logo-img"
                 src={MC_IMAGE}/>
            <p className="bold-txt">{props.restaurantName}</p>
        </div>
    );
}


export class Restaurant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            restaurantId: props.match.params.id,
            restaurantName: "",
            restaurantMenu: [],
            cart: [],
            user: {id:"user1"}
        };
    }

    componentDidMount() {
        fetch(serverPath + "getMenu/" + this.state.restaurantId)
            .then(resp => resp.json())
            .then(data => this.setState({restaurantName: data.restaurantName, restaurantMenu: data.foods}))
    }
    render() {
        return (
            <MainContent>
                <MainContentHeader>
                    <RestaurantMenuTitle restaurantName={this.state.restaurantName}/>
                </MainContentHeader>
                <MainContentBody>
                    <div className="container" style={{marginTop: "10%"}}>
                        <div className="row">
                            <div className="col-4">
                                <CartInPage />
                            </div>
                            <div className="col-8">
                                <Menu restaurantName={this.state.restaurantName} foods={this.state.restaurantMenu}/>
                            </div>
                        </div>
                    </div>
                </MainContentBody>
            </MainContent>
        );
    }
}
