package ir.ac.ut.ece.ie.repository.dao;

import ir.ac.ut.ece.ie.repository.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDao extends BaseDao {
    public static final String M2M_TABLE = Constants.ORDER_HAS_FOOD_TABLE;
    public static final String M2M_COL_ORDER = "order_id";
    public static final String M2M_COL_FOOD = "food_id";
    public static final String M2M_COL_COUNT = "_count";

    private String orderId;
    private String foodId;
    private BigDecimal count;

    @Override
    public Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> map = new HashMap<>();
        map.put(M2M_COL_ORDER, String.class);
        map.put(M2M_COL_FOOD, String.class);
        map.put(M2M_COL_COUNT, BigDecimal.class);
        return map;
    }

    @Override
    public String getAssociationTable() {
        return M2M_TABLE;
    }

    @Override
    public Object getColumnValue(String column) {
        if(column == M2M_COL_COUNT)
            return count;
        if(column == M2M_COL_FOOD)
            return foodId;
        if(column == M2M_COL_ORDER)
            return orderId;
        return null;
    }

    @Override
    public List<String> getPk() {
        return new ArrayList<>(Arrays.asList(M2M_COL_ORDER, M2M_COL_FOOD));
    }
}
