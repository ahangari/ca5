package ir.ac.ut.ece.ie.entities.status;

public enum OrderStatus {
    LOOKING_FOR_DELIVERY,
    DELIVERY_IN_WAY,
    COMPLETED;

}
