import React from 'react';
import {MainContent, MainContentBody, MainContentHeader} from "../components/content.main";
import '../../css/login_styles.css'
import {COVER_IMAGE, LOGO} from "../../images";
import SignUpForm from "./SignUpForm";

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <MainContent>
                <MainContentHeader>
                    <img src={COVER_IMAGE} className="top"/>
                    <div className="top">
                        <a href="#">
                            <img src={LOGO} className="logo"/>
                        </a>
                        <div className="description">
                            اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران
                        </div>
                    </div>
                </MainContentHeader>
                <MainContentBody>
                    <SignUpForm/>
                </MainContentBody>
            </MainContent>
        );
    }

}