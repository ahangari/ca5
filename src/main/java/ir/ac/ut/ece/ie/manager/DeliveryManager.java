package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.Utils;
import ir.ac.ut.ece.ie.entities.DeliveryEntity;
import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DeliveryManager {
    private List<DeliveryEntity> deliveries = new ArrayList<DeliveryEntity>();

    private static DeliveryManager instance;
    public static DeliveryManager getInstance() {
        if (instance == null) {
            instance = new DeliveryManager();
        }
        return instance;
    }
    private DeliveryManager(){
        try {
            renewDeliveries();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void renewDeliveries() throws IOException {
        DeliveryEntity[] deliveriesList = Utils.requestJson("http://138.197.181.131:8080/deliveries", DeliveryEntity[].class);
        deliveries = new ArrayList<>(Arrays.asList(deliveriesList));
    }


    public DeliveryEntity bestDelivery(UserEntity user, RestaurantEntity restaurant) {
        double leastTime = Double.POSITIVE_INFINITY;
        DeliveryEntity returnVal = null;
        for (DeliveryEntity delivery :
            deliveries) {
            double time = calculateTime(user.getLocation(), restaurant.getLocation(), delivery.getLocation(), delivery.getVelocity());
            if (time < leastTime) {
                leastTime = time;
                returnVal = delivery;
            }
        }
        return returnVal;
    }

    public double calculateDistance(Location userLocation, Location restaurantLocation, Location deliveryLocation) {
        double val1 = Math.pow((userLocation.getX() - restaurantLocation.getX()), 2) +
                Math.pow((userLocation.getY() - restaurantLocation.getY()), 2);
        double val2 = Math.pow((deliveryLocation.getY() - restaurantLocation.getY()), 2) +
                Math.pow((deliveryLocation.getX() - restaurantLocation.getX()), 2);
        return Math.sqrt(val1) + Math.sqrt(val2);
    }

    public double calculateTime(Location userLocation, Location restaurantLocation, Location deliveryLocation, int velocity) {
        return calculateDistance(userLocation, restaurantLocation, deliveryLocation) / velocity;
    }
}
