package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.entities.BaseEntity;
import ir.ac.ut.ece.ie.repository.dao.BaseDao;
import ir.ac.ut.ece.ie.repository.mapper.ConnectionPool;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class BaseRepository<T extends BaseEntity> extends DataBaseUtil<String> {
    protected abstract Mapper<T, String> getEntityMapper();

    public Map<String, T> loadAll(){
        try {
            return getEntityMapper().loadAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public T load(String id){
        try {
            return getEntityMapper().load(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveOrUpdate(T t) {
        boolean hasId = true;

        if(t.getId()==null){
            t.setId(this.getUUID());
            hasId = false;
        }

        T entity = this.load((String) t.getId());

        if (hasId && entity != null) {
            System.out.println(t.toString() + ".save: entity is exist!");
            try {
                getEntityMapper().update(t);
                return;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            getEntityMapper().save(t);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(String id){
        if(this.load(id)==null){
            System.out.println("BaseRepository.delete: entity not exist!");
            return;
        }

        try {
            getEntityMapper().delete(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected BaseDao convertResultSetToDao(ResultSet rs){
        return null;
    }

    public BaseDao loadDaoDoubleParam(BaseDao daoWithTwoPk){
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(loadManyToManyStatement(daoWithTwoPk))
        ) {
            try {
                ResultSet rs = st.executeQuery();
                if (!rs.next()) {
                    System.out.println("OrderRepository.loadDaoDoubleParam: entity is not exist!");
                    return null;
                }
                return convertResultSetToDao(rs);
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.loadDaoDoubleParam query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public List<BaseDao> loadDaoSingleParam(BaseDao daoWithOnePk) {
        List<BaseDao> result = new ArrayList<>();
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(loadManyToManyStatement(daoWithOnePk))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while (resultSet.next())
                    result.add(convertResultSetToDao(resultSet));
                return result;
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.loadDaoSingleParam query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveDao(BaseDao dao){
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(saveManyToManyStatement(dao))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.saveDao query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected String loadManyToManyStatement(BaseDao dao) {
        String associationTableName = dao.getAssociationTable();
        List<String> pks = dao.getPk();
        String query = " SELECT * FROM " + associationTableName + " WHERE ";
        for (String pk : pks) {
            String pkValue = (String)dao.getColumnValue(pk);
            if (pkValue!=null && !pkValue.equals(ALL))
                query += getSearchStatement(pk, dao.getColumnValue(pk), String.class) + AND;
        }
        return query.substring(0, query.length() - AND_SIZE) + SEMICOLON;
    }

    String columnsValueSql = "";
    String columnsSql = "";
    protected String saveManyToManyStatement(BaseDao dao) {
        String associationTableName = dao.getAssociationTable();
        columnsValueSql = "";
        columnsSql = "";
        Map<String, Class<?>> columns = dao.getColumns();
        columns.forEach((column, type) -> {
            columnsSql += column + COMMA;
            if(Number.class.isAssignableFrom(type))
                columnsValueSql += dao.getColumnValue(column);
            else
                columnsValueSql += formatLiteral(dao.getColumnValue(column).toString());
            columnsValueSql += COMMA;
        });
        columnsSql = columnsSql.substring(0, columnsSql.length() - COMMA_SIZE);
        columnsValueSql = columnsValueSql.substring(0, columnsValueSql.length() - COMMA_SIZE);

        return " INSERT INTO " + associationTableName + "(" +
                columnsSql + ")" + " VALUES " + "(" +
                columnsValueSql + ");";
    }

    protected String deleteManyToManyStatement(BaseDao dao) {
        String associationTableName = dao.getAssociationTable();
        List<String> pks = dao.getPk();
        String query = " DELETE FROM " + associationTableName + " WHERE ";
        for (String pk : pks) {
            if (pk != ALL)
                query += getSearchStatement(pk, (String) dao.getColumnValue(pk), String.class) + AND;
        }
        return query.substring(0, query.length() - AND_SIZE) + SEMICOLON;
    }


//    protected String saveManyToManyItemStatement();

}
