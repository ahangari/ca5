package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.Exception.LocalizedException;
import ir.ac.ut.ece.ie.entities.CartEntity;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import ir.ac.ut.ece.ie.service.util.ActionResult;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class CartManager {
    public static final String USER_TEST = "user_test";

    Map<String, CartEntity> carts = new HashMap<>();

    private static CartManager cartManager = new CartManager();

    public static CartManager getInstance() {
        return cartManager;
    }

    private CartManager() {
    }

    public boolean addOrderItemToCart(CartEntity cart, OrderItem item) throws LocalizedException {
        if (checkSameRestaurant(cart, item))
            return cart.addItem(item);
        throw new LocalizedException(this.getClass(), "addCartItemToCart", "CartItem not same in restaurant");
    }

    public boolean checkSameRestaurant(CartEntity cart, OrderItem item) {
        if (cart.getCurrentRestaurant() == null)
            return true;
        return FoodManager.getInstance().getRestaurantOfFood(item.getFood()).getId().equals(cart.getCurrentRestaurant().getId());
    }

    public CartEntity getCartOfUser(UserEntity user) {
        CartEntity returnVal = null;
        returnVal = carts.get(user.getId());
        if (returnVal == null) {
            returnVal = new CartEntity();
            carts.put(user.getId(), returnVal);
        }
        return returnVal;
    }

    public CartEntity getCartOfUser(String userId) {
        UserEntity user = UserManager.getInstance().load(userId);
        if (user != null)
            return getCartOfUser(user);
        return null;
    }

    public int calculateSumOfPrices(CartEntity cart) {
        int result = 0;
        for (OrderItem item : cart.getItems())
            result += item.getFood().getPrice().intValue() * item.getCount().intValue();
        return result;
    }

    public ActionResult finalize(CartEntity cart) {
        ActionResult result = new ActionResult();
        try {
            UserEntity user = UserManager.getInstance().load(cart.getUser().getId());
            CartEntity cartOfUser = CartManager.getInstance().getCartOfUser(user);

            for (OrderItem item : cart.getItems()) {
                CartManager.getInstance().addOrderItemToCart(cartOfUser, item);
            }

            if (cartOfUser.getItems().size() == 0) {
                result.putMsgItem("no item selected");
                result.setStatus(ActionResult.STATUS_FAILED);
                return result;
            }

            if (this.calculateSumOfPrices(cartOfUser) > user.getCredit().intValue()) {
                result.putMsgItem("not enough money in cart");
                result.setStatus(ActionResult.STATUS_FAILED);
                return result;
            }

            OrderEntity order = new OrderEntity(null, user, OrderStatus.LOOKING_FOR_DELIVERY);
            order.setItems(cart.getItems());
            OrderManager.getInstance().saveOrUpdate(order);
            UserManager.getInstance().decreaseCredit(user, this.calculateSumOfPrices(cartOfUser));
//            cart.assignDelivery();
            cart.emptyCart();
            result.setStatus(ActionResult.STATUS_OK);
            result.putMsgItem("Successfully added");
            return result;

        } catch (LocalizedException e) {
            e.printStackTrace();
            result.putMsgItem(e.getMsg());
            result.setStatus(ActionResult.STATUS_FAILED);
            return result;
        }

    }
}

