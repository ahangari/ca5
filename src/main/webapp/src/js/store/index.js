import {
    configureStore,
    getDefaultMiddleware,
} from "@reduxjs/toolkit";

import {authSlice, restaurantSlice, cartSlice} from '../reducer'

const authReducer = authSlice.reducer;
const restaurantReducer = restaurantSlice.reducer;
const cartReducer = cartSlice.reducer;

export const store = configureStore({
    reducer: {
        auth: authReducer,
        restaurant: restaurantReducer,
        cart: cartReducer
    }
});
