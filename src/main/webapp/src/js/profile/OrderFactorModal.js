import React from 'react';
import {ModalBody, ModalComponent, ModalHeader} from "../components/modal";
import OrdersFactorCore from "./OrderFactorCore";

export default class OrderFactorModal extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            order: props.order,
            modalId: props.modalId
        }
    }

    render() {
        return (
            <ModalComponent size={"large"} id={this.state.modalId}>
                <ModalHeader title={this.state.order.restaurant.name} id={this.state.modalId}/>
                <ModalBody>
                    <OrdersFactorCore factor={this.state.order}/>
                </ModalBody>
            </ModalComponent>
        );
    }

}