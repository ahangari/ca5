package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.Exception.LocalizedException;
import ir.ac.ut.ece.ie.Utils;
import ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.entities.model.FoodPartyItem;
import ir.ac.ut.ece.ie.entities.model.FoodPartyRestaurant;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.foodpartymenu.FoodPartyMenuRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FoodPartyMenuManager extends BaseManager<FoodPartyMenuEntity> {
    private static FoodPartyMenuManager instance = new FoodPartyMenuManager();

    public static FoodPartyMenuManager getInstance() {
        return instance;
    }

    private FoodPartyMenuManager() {
    }

    @Override
    protected BaseRepository getRepository() {
        return FoodPartyMenuRepository.getInstance();
    }

    public void updateFoodParty() throws LocalizedException {

        FoodPartyRestaurant[] foodParties = new FoodPartyRestaurant[0];
        try {
            foodParties = Utils.requestJson("http://138.197.181.131:8080/foodparty", FoodPartyRestaurant[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (FoodPartyRestaurant foodPartyRestaurant : foodParties) {
            if(RestaurantManager.getInstance().load(foodPartyRestaurant.getId())==null)
                throw new LocalizedException(getClass(), "updateFoodParty", "restaurant not exist");
            foodPartyRestaurant.getFoodPartyMenu().setRestaurant(foodPartyRestaurant);
            FoodPartyMenuManager.getInstance().saveOrUpdate(foodPartyRestaurant.getFoodPartyMenu());
            FoodPartyMenuRepository.getInstance().saveFoodPartyItems(foodPartyRestaurant.getFoodPartyMenu().getId(), foodPartyRestaurant.getFoodPartyMenu().getFoodPartyItems());
        }

    }

    public List<FoodPartyItem> loadLiveFoodPartyMenu() {
        try {
            this.updateFoodParty();
        }catch (LocalizedException e){
            System.out.println(e.getMsg());
            return null;
        }
        List<FoodPartyItem> result = new ArrayList<>();
        for (Map.Entry<String, FoodPartyMenuEntity> entry : super.loadAll().entrySet()) {
            RestaurantEntity restaurant = RestaurantManager.getInstance().load(entry.getValue().getRestaurant().getId());
            FoodPartyMenuRepository.getInstance().loadFoodPartyItems(entry.getKey())
                    .stream()
                    .forEach(dao -> result.add(new FoodPartyItem(
                            FoodManager.getInstance().load(dao.getFoodId()),
                            restaurant,
                            dao.getNewPrice(),
                            dao.getCount())
                    ));
        }
        return result;
    }
}
