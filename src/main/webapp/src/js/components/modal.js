import React from 'react';
export function ModalComponent(props) {
    let clazzName1 = "modal fade " + props.id;
    let clazzName2 = "modal-dialog ";
    if(props.size == "large"){
        clazzName2 += "modal-lg"
    }
    if(props.size == "small"){
        clazzName2 += "modal-sm"
    }

    return (
        <div className={clazzName1} tabIndex="-1" role="dialog"
             aria-labelledby={props.id} aria-hidden="true">
            <div className={clazzName2}>
                <div className="modal-content">
                    {props.children}
                </div>
            </div>
        </div>
    );
}

export function ModalHeader(props) {
    return (
        <div className="modal-header" style={{justifyContent: "center", color: "black"}}>
            <h5 className="modal-title" id={props.id}>{props.title}</h5>
        </div>
    );
}

export function ModalFooter(props) {
    return(
        <div className="modal-footer">
            {props.children}
        </div>
    );
}

export function ModalBody(props) {
    return (
        <div className="modal-body">
            {props.children}
        </div>
    );
}

export function BtnOpenModal(props) {
    return (
        <button type="button" className={props.clazzName}
                data-toggle="modal" data-target={"." + props.modalId} onClick={() => props.onItemClick(props.item)}>
            {props.children}
        </button>
    );
}