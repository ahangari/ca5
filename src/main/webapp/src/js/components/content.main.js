import React from 'react';
export class MainContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card text-center">
                {this.props.children}
            </div>
        );
    }
}

export function MainContentHeader(props) {
    return (
        <div className="card-header" style={{padding: 0}}>
            {props.children}
        </div>
    );
}

export function MainContentBody(props) {
    return (
        <div className="card-body CardBodyStyle">
            {props.children}
        </div>
    );
}