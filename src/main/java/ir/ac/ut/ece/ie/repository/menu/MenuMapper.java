package ir.ac.ut.ece.ie.repository.menu;

import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.MenuEntity.*;


public class MenuMapper extends Mapper<MenuEntity, String> {
    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, MenuEntity menuEntity) {
        switch (column){
            case COL_ID:
                return menuEntity.getId();
            default:
                return null;
        }    }

    @Override
    protected MenuEntity convertResultSetToObject(ResultSet rs) throws SQLException {
        return new MenuEntity(rs.getString(MenuEntity.COL_ID), null);
    }

    @Override
    public String getDefaultTable() {
        return Constants.MENU_TABLE;
    }
}
