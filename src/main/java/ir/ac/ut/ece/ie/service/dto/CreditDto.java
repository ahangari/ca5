package ir.ac.ut.ece.ie.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import ir.ac.ut.ece.ie.entities.UserEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreditDto {
    String credit;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    UserEntity userEntity;
}
