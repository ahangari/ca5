import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import React from "react";
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {store} from '../src/js/store'
import Application from './js/application';
import {Route, HashRouter} from 'react-router-dom';

window.serverPath = "http://185.166.105.6:30361/IEREACT/";

ReactDOM.render(
    <>
        <Provider store={store}>
            <HashRouter>
                <Route path="/" component={Application}/>
            </HashRouter>
        </Provider>
    </>
    , document.getElementById("app"));