package ir.ac.ut.ece.ie.entities;

import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity extends BaseEntity<String> {
    public static final String COL_ID = "id";
    public static final String COL_USER = "User_id";
    public static final String COL_STATUS = "status";

    private String id;
    private UserEntity user;
    private OrderStatus status;
    private List<OrderItem> items;

    public OrderEntity(String id, UserEntity user, OrderStatus status){
        this.id = id;
        this.user = user;
        this.status = status;
    }
    
}
