package ir.ac.ut.ece.ie.service.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ActionResult {
    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAILED = "FAILED";

    String message;
    String status;
    public ActionResult(){
    }

    public void putMsgItem(String msg){
        message += "{" + msg + "}";

    }

    public void setStatus(String status){
        this.status = status;
    }

    public ActionResult(String msg){
        message = msg;
    }
}
