package ir.ac.ut.ece.ie.repository.mapper;

import ir.ac.ut.ece.ie.entities.BaseEntity;
import ir.ac.ut.ece.ie.repository.DataBaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Mapper<T extends BaseEntity, I> extends DataBaseUtil implements IMapper<T, I> {
    protected String getLoadStatement(I id) {
        return "SELECT " + "*" +
                " FROM " + getDefaultTable() +
                " WHERE id = " + formatLiteral(id) + ";";
    }

    protected String getDeleteStatement(I id) {
        return "DELETE FROM " + getDefaultTable() +
                " WHERE id = " + formatLiteral(id) + ";";
    }

    protected String getLoadAllStatement() {
        return "SELECT *" +
                " FROM " + getDefaultTable() +
                ";";
    }

    protected String getSaveStatement(T t) {
        String saveColumnsQuery = "";
        String saveValuesQuery = "";
        Map<String, Class<?>> columns = getColumns();
        for (Map.Entry<String, Class<?>> entry : columns.entrySet()) {
            saveColumnsQuery += entry.getKey() + COMMA;
            if (!Number.class.isAssignableFrom(entry.getValue()))
                saveValuesQuery += formatLiteral(getColmunValue(entry.getKey(), t)) + COMMA;
            else
                saveValuesQuery += (Number) getColmunValue(entry.getKey(), t) + COMMA;
        }
        saveColumnsQuery = saveColumnsQuery.substring(0, saveColumnsQuery.length() - COMMA_SIZE);
        saveValuesQuery = saveValuesQuery.substring(0, saveValuesQuery.length() - COMMA_SIZE);

        return "INSERT INTO " + getDefaultTable() +
                "(" + saveColumnsQuery +
                ")" + " VALUES " + "(" + saveValuesQuery + ");";
    }

    protected abstract Map<String, Class<?>> getColumns();

    protected abstract Object getColmunValue(String column, T t);

    protected String getUpdateStatement(T t) {
        String updateColumnQuery = "";
        String whereQuery = "";
        Map<String, Class<?>> columns = getColumns();
        for (Map.Entry<String, Class<?>> entry: columns.entrySet()) {
            updateColumnQuery += getSearchStatement(entry.getKey(), getColmunValue(entry.getKey(), t), entry.getValue())
                    + COMMA;
        }
        updateColumnQuery = updateColumnQuery.substring(0, updateColumnQuery.length() - COMMA_SIZE);
        String COL_ID = "id";
        whereQuery = getSearchStatement(COL_ID, getColmunValue(COL_ID, t), String.class);

        return "UPDATE " + getDefaultTable() + " SET " + updateColumnQuery + " WHERE " + whereQuery + ";";
    }

    abstract protected T convertResultSetToObject(ResultSet rs) throws SQLException;

    public Map<I, T> loadAll() throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getLoadAllStatement())
        ) {
            ResultSet resultSet;
            try {
                Map<I, T> result = new HashMap<>();
                resultSet = st.executeQuery();
                while (resultSet.next()) {
                    T obj = convertResultSetToObject(resultSet);
                    result.put((I) obj.getId(), obj);
                }
                return result;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.findByID query.");
                System.out.println(st);
                throw ex;
            }
        }
    }

    ;

    public T load(I id) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getLoadStatement(id))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                if (!resultSet.next()) {
                    System.out.println("Mapper.load: Entity is not exist!");
                    return null;
                }
                return convertResultSetToObject(resultSet);
            } catch (SQLException ex) {
                System.out.println("error in Mapper.findByID query.");
                System.out.println(st);
                throw ex;
            }
        }
    }

    public void update(T obj) throws SQLException {

        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getUpdateStatement(obj))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.update query.");
                System.out.println(st);
                throw ex;
            }
        }
    }

    public void save(T obj) throws SQLException {

        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getSaveStatement(obj))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.save query.");
                System.out.println(st);
                throw ex;
            }
        }
    }

    public void delete(I id) throws SQLException {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(getDeleteStatement(id))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in Mapper.delete query.");
                throw ex;
            }
        }
    }

    public abstract String getDefaultTable();

    public List<T> getEntitiesByProp(String prop, String value) {
        List<T> result = new ArrayList<>();
        String statement = "SELECT " + "*" + " FROM " + getDefaultTable() +
                " Where " + prop + " = " + formatLiteral((I) value);
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(statement);
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while (resultSet.next())
                    result.add(convertResultSetToObject(resultSet));
                return result;
            } catch (SQLException ex) {
                System.out.println("error in Mapper.findByID query.");
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}