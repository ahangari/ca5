package ir.ac.ut.ece.ie.repository.order;

import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.OrderEntity.*;

public class OrderMapper extends Mapper<OrderEntity, String> {

    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        columns.put(COL_USER, String.class);
        columns.put(COL_STATUS, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, OrderEntity orderEntity) {
        switch (column){
            case COL_ID:
                return orderEntity.getId();
            case COL_USER:
                return orderEntity.getUser().getId();
            case COL_STATUS:
                return orderEntity.getStatus().name();
            default:
                return null;
        }
    }

    @Override
    protected OrderEntity convertResultSetToObject(ResultSet rs) throws SQLException {
        return new OrderEntity(rs.getString(COL_ID), new UserEntity(rs.getString(COL_USER)),
                OrderStatus.valueOf(rs.getString(COL_STATUS)));
    }

    @Override
    public String getDefaultTable() {
        return Constants.ORDER_TABLE;
    }
}
