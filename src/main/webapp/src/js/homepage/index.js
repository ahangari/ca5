import React from 'react';
import {Redirect} from 'react-router-dom';

export default class WelcomePage extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Redirect to={{pathname: '/dashboard', state: {from: this.props.location}}}/>
        );
    }

}