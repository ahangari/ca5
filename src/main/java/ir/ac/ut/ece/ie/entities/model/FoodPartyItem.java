package ir.ac.ut.ece.ie.entities.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FoodPartyItem {

    @JsonUnwrapped
    private FoodEntity food;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private RestaurantEntity restaurant;
    @JsonProperty(value = "oldPrice")
    private BigDecimal newPrice;
    @JsonProperty(value = "count")
    private BigDecimal count;
}
