package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.dao.OrderItemDao;
import ir.ac.ut.ece.ie.repository.order.OrderRepository;

import java.util.ArrayList;
import java.util.List;

public class OrderManager extends BaseManager<OrderEntity> {
    private static OrderManager instance = new OrderManager();
    public static OrderManager getInstance(){
        return instance;
    }
    private OrderManager(){
        super.loadAll();
    }


    @Override
    public void saveOrUpdate(OrderEntity order){
        super.saveOrUpdate(order);
        this.saveOrderItems(order.getId(), order.getItems());
    }

    public void saveOrderItems(String orderId, List<OrderItem> items){
        OrderRepository.getInstance().saveOrderItems(orderId, items);
    }

    public List<OrderItem> loadOrderItems(String orderId){
        List<OrderItem> orderItems = new ArrayList<>();
        List<OrderItemDao> daos =  OrderRepository.getInstance().loadDaoByOrderId(orderId);
        daos.stream()
                .filter(item -> item.getOrderId().equals(orderId))
                .forEach(item -> orderItems.add(new OrderItem(FoodManager.getInstance().load(item.getFoodId()), item.getCount())));
        return orderItems;
    }

    public List<OrderEntity> getOrdersByUserId(String userId){
        return OrderRepository.getInstance().getOrdersByUserId(userId);
    }

    public RestaurantEntity getRestaurantOfOrder(OrderEntity order){
        FoodEntity food = this.loadOrderItems(order.getId()).get(0).getFood();
        return FoodManager.getInstance().getRestaurantOfFood(food);
    }

    @Override
    protected BaseRepository getRepository() {
        return OrderRepository.getInstance();
    }
}
