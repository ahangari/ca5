package ir.ac.ut.ece.ie.entities;

public enum Status {
    WAITING_DELIVERY, IN_WAY, DONE
}
