package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.entities.BaseEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseManager<T extends BaseEntity> {
    protected Map<String, T> entites = new HashMap<>();

    public void saveOrUpdate(T t){
        getRepository().saveOrUpdate(t);
    }

    public T load(String id){
        if(entites.containsKey(id))
            return entites.get(id);
        T obj = (T) getRepository().load(id);
        entites.put (id, obj);
        return obj;
    }

    public Map<String, T> loadAll(){
        entites = getRepository().loadAll();
        return entites;
    }

    public void delete(String id){
        getRepository().delete(id);
    }

    public String hashString(String str){
        return DigestUtils.sha256Hex(str);
    }

    protected abstract BaseRepository getRepository();
}

