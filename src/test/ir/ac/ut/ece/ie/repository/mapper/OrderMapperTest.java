package ir.ac.ut.ece.ie.repository.mapper;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import ir.ac.ut.ece.ie.repository.TestConnectionPool;
import ir.ac.ut.ece.ie.repository.TestUtil;
import ir.ac.ut.ece.ie.repository.order.OrderMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class OrderMapperTest extends TestUtil {
    private final String MENU_ID = "menu_test";
    private final String USER_ID = "user_test";
    private final String FOOD_ID = "food_test";
    private final String ORDER_ID = "order1";

    private Connection connection;
    private OrderMapper orderMapper;
    private MenuEntity menuEntity;
    private UserEntity userEntity;
    private FoodEntity foodEntity;

    private final OrderStatus STATUS = OrderStatus.LOOKING_FOR_DELIVERY;


    @Before
    public void setUp() throws Exception {
        connection = TestConnectionPool.getConnection();
        orderMapper = new OrderMapper();
        menuEntity = saveMenu(MENU_ID);
        userEntity = saveUser(USER_ID);
        foodEntity = saveFood(FOOD_ID, menuEntity);
    }

    @Test
    public void testSaveEntity() throws SQLException {
        OrderEntity expected = new OrderEntity(ORDER_ID, userEntity, STATUS);
        orderMapper.save(expected);
        OrderEntity actual = orderMapper.load(ORDER_ID);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getUser().getId(), actual.getUser().getId());
        assertEquals(expected.getStatus(), actual.getStatus());
    }

    @After
    public void tearDown() throws Exception {
        orderMapper.delete(ORDER_ID);
        connection.close();
    }
}