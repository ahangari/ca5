package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ir.ac.ut.ece.ie.entities.model.FoodPartyItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FoodPartyMenuEntity extends BaseEntity<String>{
    public static final String COL_ID = "id";
    public static final String COL_RESTAURANT = "restaurant_id";


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    @JsonProperty("menu")
    private List<FoodPartyItem> foodPartyItems;

    @JsonIgnore
    private RestaurantEntity restaurant;
}
