package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryEntity {
    private String id;
    private int velocity;
    private Location location;
    @JsonIgnore
    private DeliveryStatus status;
}
