package ir.ac.ut.ece.ie.repository.mapper;

import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.TestConnectionPool;
import ir.ac.ut.ece.ie.repository.menu.MenuMapper;
import ir.ac.ut.ece.ie.repository.restaurant.RestaurantMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class RestaurantMapperTest {

    private RestaurantEntity test;
    private RestaurantMapper restaurantMapper;
    private MenuMapper menuMapper;
    private Connection connection;

    private final String ID = "restaurant1";
    private final String NAME = "atf";
    private final String LOGO = "sample_logo";
    private final Location LOCATION = new Location(10, 20);
    private final MenuEntity MENU = new MenuEntity("menu1", null);

    @Before
    public void setUp() throws Exception {
        restaurantMapper = new RestaurantMapper();
        menuMapper = new MenuMapper();
        connection = TestConnectionPool.getConnection();
        menuMapper.save(MENU);
    }

    @Test
    public void testSaveEntity() throws SQLException {
        test = new RestaurantEntity(ID, NAME, LOCATION, LOGO, MENU);
        restaurantMapper.save(test);
        RestaurantEntity actual = restaurantMapper.load(ID);
        RestaurantEntity expected = new RestaurantEntity(ID, NAME, LOCATION, LOGO, MENU);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getLogo(), actual.getLogo());
        assertEquals(expected.getLocation().toString(), actual.getLocation().toString());
        assertEquals(expected.getMenu().getId(), actual.getMenu().getId());
    }

    @After
    public void tearDown() throws Exception {
        restaurantMapper.delete(ID);
        menuMapper.delete(MENU.getId());
        connection.close();
    }
}