package ir.ac.ut.ece.ie.service.dto;

import ir.ac.ut.ece.ie.entities.model.FoodPartyItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodPartyDto {
    List<FoodPartyItem> foodPartyItems;
}
