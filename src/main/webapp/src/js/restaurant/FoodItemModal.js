import React from 'react';
import {connect} from 'react-redux';
import {ModalBody, ModalComponent, ModalFooter, ModalHeader} from "../components/modal";
import {cartSlice} from "../reducer";
import {FOOD_MODAL_ID} from "../Constants";
import {FOOD_IMAGE} from "../../images";

const {addItem} = cartSlice.actions;



class FoodItemModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalId: FOOD_MODAL_ID,
            count: 1
        }

    }
    increaseCount(){
        this.setState({count: this.state.count +1})
    }
    decreaseCount(){
        if(this.state.count != 0)
            this.setState({count: this.state.count - 1})
    }
    finalizeItemToCart(){
        this.props.addItem({food: this.props.food, count: this.state.count});
    }
    render() {
        return (
            <div>
                <ModalComponent size={"large"} id={this.state.modalId}>
                    <ModalHeader title={this.props.restaurantName} id={this.state.modalId}/>
                    <ModalBody>
                        <div className="row">
                            <div className="col-4">
                                <img src={FOOD_IMAGE} style={{width: "180px", height: "150px"}}/>
                            </div>
                            <div className="col-8" style={{textAlign: "start"}}>
                                <div className="bold-txt"
                                     style={{fontSize: "25px", margin: "2%"}}>{this.props.food.name}</div>
                                <div style={{margin: "2%"}}>{this.props.food.description}</div>
                                <div style={{fontSize: "25px", margin: "2%"}}>{this.props.food.price * this.state.count} تومان</div>
                            </div>
                        </div>
                        <div>
                            <ModalFooter>
                                <div className="row col-6">
                                    <div className="col-4">
                                        <CounterForItem count={this.state.count} increaseCount={this.increaseCount.bind(this)} decreaseCount={this.decreaseCount.bind(this)}/>
                                    </div>
                                    <div className="col-8">
                                        <button type="button" className="btn btn-info btn-lg btn-block btn-add-cart"
                                                style={{
                                                    backgroundColor: "#4ECDC4",
                                                    fontSize: "18px",
                                                }} onClick={()=> this.finalizeItemToCart()} data-dismiss="modal">
                                            {"اضافه کردن به سبد خرید"}
                                        </button>
                                    </div>
                                </div>
                            </ModalFooter>
                        </div>
                    </ModalBody>
                </ModalComponent>
            </div>
        );
    }
}

export function CounterForItem(props) {
    return (
        <div className="row justify-content-center">
            <a className="flaticon-plus" style={{color: "green", textAlign: "center"}}
               onClick={() => props.increaseCount()}></a>
            <span className="cart-food-counter-text">{props.count}</span>
            <a className="flaticon-minus" style={{color: "red"}} onClick={() => props.decreaseCount()}></a>
        </div>
    );
}

export default connect(null,{addItem})(FoodItemModal);