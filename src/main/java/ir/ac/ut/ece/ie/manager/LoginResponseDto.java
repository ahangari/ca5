package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponseDto {
    UserEntity userEntity;
    String jwtToken;
}
