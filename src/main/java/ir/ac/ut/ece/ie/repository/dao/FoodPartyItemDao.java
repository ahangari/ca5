package ir.ac.ut.ece.ie.repository.dao;

import ir.ac.ut.ece.ie.repository.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FoodPartyItemDao extends BaseDao {
    public static final String M2M_TABLE = Constants.FOODPARTY_HAS_FOOD_TABLE;
    public static final String M2M_COL_FOODPARTY_MENU_ID = "foodparty_menu_id";
    public static final String M2M_COL_FOOD_ID = "food_id";
    public static final String M2M_COL_NEW_PRICE = "new_price";
    public static final String M2M_COL_COUNT = "_count";

    private String foodPartyMenuId;
    private String foodId;
    private BigDecimal newPrice;
    private BigDecimal count;

    @Override
    public Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(M2M_COL_FOODPARTY_MENU_ID, String.class);
        columns.put(M2M_COL_FOOD_ID, String.class);
        columns.put(M2M_COL_NEW_PRICE, BigDecimal.class);
        columns.put(M2M_COL_COUNT, BigDecimal.class);
        return columns;
    }

    @Override
    public String getAssociationTable() {
        return M2M_TABLE;
    }

    @Override
    public Object getColumnValue(String column) {
        if(column == M2M_COL_FOOD_ID)
            return foodId;
        if(column == M2M_COL_FOODPARTY_MENU_ID)
            return foodPartyMenuId;
        if (column == M2M_COL_NEW_PRICE)
            return newPrice;
        if (column == M2M_COL_COUNT)
            return count;
        return null;
    }

    @Override
    public List<String> getPk() {
        return new ArrayList<>(Arrays.asList(M2M_COL_FOODPARTY_MENU_ID, M2M_COL_FOOD_ID));
    }
}
