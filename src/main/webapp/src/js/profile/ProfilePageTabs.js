import React from 'react';
import {connect} from 'react-redux';
import OrderItem from "./OrderItem";
import TabContainer from "../components/TabContainer";
import OrderFactorModal from "./OrderFactorModal";

const INCREASE_AMOUNT_FA = "میزان افزایش اعتبار";
const INCREASE_FA = "افزایش";
const FIRST = "1";
const SECOND = "2";

export default class ProfilePageTabs extends React.Component {
    render() {
        return (
            <div className="container" style={{marginTop: "4%"}}>
                <Tabs/>
            </div>
        );
    }
}

class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabSelectedNumber: 1
        }
    }

    onTabNavClick = (tabSelectedNumber) => {
        this.setState({tabSelectedNumber: tabSelectedNumber})
    }

    render() {
        return (
            <div>
                {this.props.children}
                <TabNavigation onTabNavClick={this.onTabNavClick.bind(this)}/>
                <TabBody tabSelectedNumber={this.state.tabSelectedNumber}/>
            </div>
        );
    }
}

class TabNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstSelected: true
        }
    }

    changeSelected(first) {
        if (this.state.firstSelected && !first) {
            this.setState({firstSelected: false});
            this.props.onTabNavClick(FIRST);
        }


        if (!this.state.firstSelected && first) {

            this.setState({firstSelected: true});
            this.props.onTabNavClick(SECOND);
        }
    }

    render() {
        return (
            <ul className="nav nav-pills mb-3 CreditNavigationStyle" id="pills-tab" role="tablist">
                <NavItem onTabClick={this.changeSelected.bind(this)} selected={this.state.firstSelected} name={"orders"}
                         title={"سفارش ها"} first={true}/>
                <NavItem onTabClick={this.changeSelected.bind(this)} selected={!this.state.firstSelected}
                         name={"credit"} title={"افزایش اعتبار"} first={false}/>
            </ul>
        );
    }
}

class TabBody extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            credit: 0,
            user: "user1",
            orders: [],
            modalId: "mylargeModal",
            orderShowModal: null
        }
    }

    isActive(id) {
        if (this.props.tabSelectedNumber == id)
            return true;
        else
            return false
    }

    componentDidMount() {
        fetch(serverPath + 'getOrders/' + this.state.user, {
            headers: {
                Authorization: 'Bearer ' + this.props.auth.jwtToken
            }
        })
            .then(resp => resp.json())
            .then(data => this.setState({orders: data.orders}))
    }

    increaseCredit(event) {
        event.preventDefault();
        const requestOptions = {
            method: 'POST',
            mode: 'cors',
            headers: {
                "Content-Type": "application/json",
                Authorization: 'Bearer ' + this.props.auth.jwtToken
            },
            body: JSON.stringify({credit: this.state.credit})
        };
        fetch(serverPath + 'increseCredit', requestOptions)
            .then(resp => resp.json())
            .then(data => alert(data.message))
    }

    handleChange() {
        this.setState({credit: event.target.value})
    }

    onOrderClick(order) {
        this.setState({orderShowModal: order});
    }

    render() {
        return (
            <div>
                <div className="tab-content border tabs-container-style" id="pills-tabContent">
                    <TabContainer name={"increase"} active={this.isActive(FIRST)} show={this.isActive(FIRST)}>
                        <div className="row input-btn-row">
                            <input type="text" className="border input-style" placeholder={INCREASE_AMOUNT_FA}
                                   value={this.state.credit} onChange={() => this.handleChange()}/>
                            <button type="button" className="btn border  increaseCreditBtnStyle"
                                    onClick={() => this.increaseCredit(event)}> {INCREASE_FA}</button>
                        </div>
                    </TabContainer>
                    <TabContainer name={"orders"} active={this.isActive(SECOND)} show={this.isActive(SECOND)}>
                        {
                            this.state.orders ? this.state.orders.map((value, index) => {
                                return <OrderItem order={value} onOrderClick={this.onOrderClick.bind(this)}/>
                            }) : "سفارشی ندراد"
                        }
                        {this.state.orderShowModal ?
                            <OrderFactorModal order={this.state.orderShowModal} modalId={this.state.modalId}/>
                            : null}
                    </TabContainer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}
connect(mapStateToProps)(TabBody);

const NavItem = (props) => {

    const href = (name) => {
        return "#pills-" + name;
    }
    const ariaControls = (name) => {
        return "pills-" + name;
    }
    const iden = (name) => {
        return "pills-" + {name} + "-tab";
    }
    return (
        <li className="nav-item btn border"
            style={{width: "250px", backgroundColor: props.selected ? "#FF6B6B" : "white"}}>
            <a className="nav-link" id={iden(props.name)} data-toggle="pill"
               href={href(props.name)} role="tab"
               aria-controls={ariaControls(props.name)} aria-selected={props.selected}
               style={{color: props.selected ? "white" : "black"}}
               onClick={() => props.onTabClick(props.first)}>
                {props.title}</a>
        </li>
    );
}

