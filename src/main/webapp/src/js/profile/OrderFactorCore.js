import React from 'react';

export default function OrdersFactorCore(props) {
    var itemCounter = 1;
    var items = props.factor.items;
    var totalPrice = 0;
    items.map((item) => {
        totalPrice += item.count * item.food.price;
    })
    return (
        <div>
            <table className="table table-bordered">
                <thead>
                <tr className={"table-secondary"}>
                    <td scope="col">ردیف</td>
                    <td scope="col">نام غذا</td>
                    <td scope="col">تعداد</td>
                    <td scope="col">قیمت</td>
                </tr>
                </thead>
                <tbody>
                {
                    items.map((item) => {
                        return <OrderDetailItem foodName={item.food.name} count={item.count} price={item.food.price}>
                            <th>{itemCounter++}</th>
                        </OrderDetailItem>;
                    })
                }
                </tbody>
            </table>
            <div className="total-price" style={{textAlign: "end"}}>
                جمع کل:{totalPrice}
            </div>
        </div>
    );
}

function OrderDetailItem(props) {
    return (
        <tr>
            {props.children}
            <th>{props.foodName}</th>
            <th>{props.count}</th>
            <th>{props.price}</th>
        </tr>
    );
}