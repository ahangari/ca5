package ir.ac.ut.ece.ie.entities.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodPartyRestaurant extends RestaurantEntity {

    FoodPartyMenuEntity foodPartyMenu;

    @JsonProperty("menu")
    private void unpackListOfFood(List<FoodPartyItem> foodPartyItems) {
        FoodPartyMenuEntity menu = new FoodPartyMenuEntity(null, foodPartyItems, null);
        this.foodPartyMenu = menu;
    }
}
