package ir.ac.ut.ece.ie.entities.model;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem {
    FoodEntity food;
    BigDecimal count;
}
