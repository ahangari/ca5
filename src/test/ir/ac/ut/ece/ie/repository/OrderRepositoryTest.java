package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.repository.food.FoodRepository;
import ir.ac.ut.ece.ie.repository.menu.MenuRepository;
import ir.ac.ut.ece.ie.repository.order.OrderRepository;
import ir.ac.ut.ece.ie.repository.user.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderRepositoryTest extends TestUtil{

    public static final String MENU_ID = "menu_test";
    public static final String USER_ID = "user_test";
    public static final String FOOD_ID_1 = "food_test";
    public static final String ORDER_ID = "order_test";

    Connection connection;
    @Before
    public void setUp() throws SQLException {
        connection = TestConnectionPool.getConnection();
        MenuEntity menu = saveMenu(MENU_ID);
        UserEntity userEntity = saveUser(USER_ID);
        saveFood(FOOD_ID_1, menu);
        saveOrder(ORDER_ID, userEntity);
    }

//    @Test
//    public void getOrderItems() {
//    }

    @Test
    public void loadDaoById(){
        List<OrderItem> orderItems = new ArrayList<>();
        FoodEntity food = new FoodEntity(FOOD_ID_1);
        OrderItem orderItem = new OrderItem(food, new BigDecimal(10));
        orderItems.add(orderItem);
        OrderRepository.getInstance().saveOrderItems(ORDER_ID, orderItems);
//        OrderRepository.getInstance().deleteDao(expected);
    }

    @After
    public void tearDown() throws Exception {
        FoodRepository.getInstance().delete(FOOD_ID_1);
        OrderRepository.getInstance().delete(ORDER_ID);
        MenuRepository.getInstance().delete(MENU_ID);
        UserRepository.getInstance().delete(USER_ID);
        connection.close();
    }

//    @Test
//    public void test(){
//        FoodEntity food1 = new FoodEntity("food1");
//        FoodEntity food2 = new FoodEntity("food2");
//        FoodRepository.getInstance().save(food1);
//        FoodRepository.getInstance().save(food2);
//        OrderEntity orderEntity = new OrderEntity("order1", new UserEntity("user1"), Status.DONE);
//        OrderRepository.getInstance().save(orderEntity);
//    }
}