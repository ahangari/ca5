import React from 'react';
import {connect} from 'react-redux';
import CartFoodItem from "./CartFoodItem";
import {cartSlice, finalizeCart} from "../reducer";

const {changeFoodCount} = cartSlice.actions;

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: this.props.items,
            user: this.props.user
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.result.message!=this.props.result.message)
            alert(JSON.stringify(nextProps.result));
        this.setState({items: nextProps.items});
    }

    totalPrice() {
        let totalPrice = 0;
        if (this.state.cart) {
            this.state.cart.map((value, index) => {
                totalPrice += value.food.price * value.count;
            })
        }
        return totalPrice;
    }

    render() {
        return (
            <div>
                <div className='cart-foods-div'>
                    {
                        this.state.items ? this.state.items.map((value, index) => {
                                return <CartFoodItem food={value.food} count={value.count}/>;
                            }) :
                            "سبد خالی است"
                    }
                </div>
                <div className="total-price" style={{textAlign: "center", margin: "5%", fontWeight: "bold"}}>
                    {this.totalPrice()}تومان
                </div>
                <div className="finalize-cart-btn">
                    <button type="button" className="btn btn-info"
                            onClick={() => this.props.finalizeCart(this.state.items, this.state.user)}>تایید نهایی
                    </button>
                </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        items: state.cart.items,
        user: state.auth.user,
        result: state.cart.result
    };
}

export default connect(mapStateToProps, {
    changeFoodCount,
    finalizeCart
})(Cart);
