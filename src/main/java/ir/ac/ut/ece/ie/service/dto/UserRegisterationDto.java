package ir.ac.ut.ece.ie.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterationDto {
    private String firstname;
    private String lastname;
    private String password;
    private String email;
}
