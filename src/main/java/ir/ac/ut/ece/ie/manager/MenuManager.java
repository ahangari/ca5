package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.menu.MenuRepository;

import java.util.List;

public class MenuManager extends BaseManager<MenuEntity> {
    private List<MenuEntity> menus;
    private static MenuManager instance = new MenuManager();

    private MenuManager(){}

    public static MenuManager getInstance(){
        return instance;
    }

    public RestaurantEntity getRestaurant(MenuEntity menuEntity){
        return RestaurantManager.getInstance().getRestaurantByMenu(menuEntity);
    }

    @Override
    public MenuEntity load(String id){
        MenuEntity menu = super.load(id);
        if(menu.getFoods()==null)
            menu.setFoods(FoodManager.getInstance().getFoodsByMenu(id));
        return menu;
    }

    @Override
    protected BaseRepository getRepository() {
        return MenuRepository.getInstance();
    }
}
