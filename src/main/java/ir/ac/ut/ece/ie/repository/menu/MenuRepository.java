package ir.ac.ut.ece.ie.repository.menu;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.food.FoodRepository;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;
import ir.ac.ut.ece.ie.repository.restaurant.RestaurantRepository;

import java.sql.SQLException;
import java.util.List;

public class MenuRepository extends BaseRepository<MenuEntity> {
    private static MenuRepository instance = new MenuRepository();
    private MenuMapper menuMapper;
    public static MenuRepository getInstance(){
        return instance;
    }
    private MenuRepository(){
        menuMapper = new MenuMapper();
    }

    public MenuEntity getMenuById(String id) throws SQLException {
        return this.menuMapper.load(id);
    }

    public List<FoodEntity> getFoods(String id){
        return FoodRepository.getInstance().getFoodsByMenu(id);
    }

    public RestaurantEntity getRestaurant(String id){
        return RestaurantRepository.getInstance().getRestaurantByMenu(id);
    }

    @Override
    protected Mapper<MenuEntity, String> getEntityMapper() {
        return new MenuMapper();
    }
}
