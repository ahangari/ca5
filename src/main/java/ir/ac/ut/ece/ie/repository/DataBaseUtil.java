package ir.ac.ut.ece.ie.repository;

import java.util.UUID;

public abstract class DataBaseUtil<I> {
    protected static final String ALL = "all";
    protected static final String COMMA = ",";
    protected static final char SEMICOLON = ';';
    protected static final char QUOT = '\'';
    protected static final String AND = " and ";
    protected static final int COMMA_SIZE = 1;
    protected static final int AND_SIZE = 5;

    protected String getSearchStatement(String key, Object value, Class<?> valuetype)
    {
        if(Number.class.isAssignableFrom(valuetype))
            return key + " = " + (Number) value;
        return key + " = " + formatLiteral((I) value);
    }

    protected String formatLiteral(I literal) {
        if(literal!=null)
            return QUOT + literal.toString() + QUOT;
        return null;
    }
    protected String getUUID(){
        return UUID.randomUUID().toString();
    }

}
