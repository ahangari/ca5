import React from 'react';
import '../../css/profile.credit.css';

import {MainContent, MainContentHeader, MainContentBody} from '../components/content.main';
import ProfileDetail from "./ProfileDetail";
import ProfilePageTabs from "./ProfilePageTabs";

export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: {}
        }
    }

    componentDidMount() {
        fetch(serverPath + 'user/' + 'user1')
            .then(resp => resp.json())
            .then(data => this.setState({profile: data}))
        console.log(this.state.profile)
    }

    render() {
        return (
            <MainContent>
                <MainContentHeader>
                    <ProfileDetail
                        fullName={this.state.profile.firstName + this.state.profile.lastName}
                        phoneNumber={this.state.profile.phoneNumber}
                        email={this.state.profile.email}
                        credit={this.state.profile.credit}/>
                </MainContentHeader>
                <MainContentBody>
                    <div className="container" style={{marginTop: "4%"}}>
                        <ProfilePageTabs/>
                    </div>
                </MainContentBody>
            </MainContent>
        );
    }
}
