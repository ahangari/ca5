import React, {Component} from 'react';
import {LicenceInCard} from './header_footer/LicenceComponent';
import NavigationBar from './header_footer/NavigationBar'
import Profile from "./profile";
import {Route, Redirect} from 'react-router-dom';
import {Restaurant} from "./restaurant";
import {CartModal} from "./cart/CartModal";
import {connect} from 'react-redux';
import Login from "./user/Login";
import SignUp from "./user/SignUp";
import RestaurantsPage from "./homepage/RestaurantsPage";
import WelcomePage from "./homepage";
import Logout from "./user/Logout";

class Application extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authed: this.props.auth.jwtToken
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.auth.jwtToken)
            this.setState({authed: nextProps.auth.jwtToken})
    }

    // getSession() {
    //     const jwt = this.props.auth.jwtToken;
    //     console.log(jwt);
    //     return;
    //     let session;
    //     try {
    //         if (jwt) {
    //             const base64Url = jwt.split('.')[1]
    //             const base64 = base64Url.replace('-', '+').replace('_', '/')
    //             session = JSON.parse(window.atob(base64))
    //         }
    //     } catch (error) {
    //         console.log(error)
    //     }
    //     return session;
    // }

    render() {
        return (
            <div>
                <NavigationBar authed={this.state.authed}/>
                <WelcomePage/>
                <Route path="/login" component={Login}/>
                <Route path="/signup" component={SignUp}/>
                <Route path="/logout" component={Logout}/>
                <PrivateRoute authed={this.state.authed} path="/profile" component={Profile}/>
                <PrivateRoute authed={this.state.authed} path="/menu/:id" component={Restaurant}/>
                <PrivateRoute authed={this.state.authed} path="/dashboard" component={RestaurantsPage}/>
                <CartModal authed={this.state.authed}/>
                <LicenceInCard/>
            </div>
        );
    }
}

function PrivateRoute({component: Component, authed, ...rest}) {
    return (
        <Route
            {...rest}
            render={(props) => authed
                ? <Component {...props} />
                : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>}
        />
    )
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps, {})(Application)