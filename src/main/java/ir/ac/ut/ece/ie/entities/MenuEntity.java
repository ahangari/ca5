package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MenuEntity extends BaseEntity<String> {
    public static final String COL_ID = "id";

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String id;

    protected List<FoodEntity> foods = new ArrayList<FoodEntity>();

    public MenuEntity(List<FoodEntity> foods) {
        this.foods = foods;
    }

    public MenuEntity(String id){
        this.id = id;
    }

    public List<FoodEntity> getFoods() {
        return foods;
    }


    public void add_food(FoodEntity food) {
        foods.add(food);
    }

    public boolean menuHasFood(String foodName) {
        for (FoodEntity food: foods) {
            if (food.getName().equals(foodName))
                return true;
        }
        return false;
    }

    public void addFood(FoodEntity food) {
        foods.add(food);
    }
}
