import React from 'react';
import {BtnOpenModal} from "../components/modal";
import {FOOD_MODAL_ID} from "../Constants";
import {FOOD_IMAGE} from "../../images";

export default class Food extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className="shadow-box restaurant-menu-item">
                <img src={FOOD_IMAGE} style={{width: "80px", height: "80px"}}/>
                <div style={{fontWeight: "bold", textAlign: "center"}}>{this.props.item.name}</div>
                <div style={{textAlign: "center", fontSize: "11px"}}>{this.props.item.price}</div>
                <BtnOpenModal modalId={FOOD_MODAL_ID} clazzName={"btn btn-add-cart"} item={this.props.item} onItemClick={this.props.onItemClick}>
                    افزودن به سبد خرید
                </BtnOpenModal>
            </div>
        );
    }
}

