import React from 'react';

export default class OrderItem extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            order: props.order,
            modalId: "mylargeModal"
        }
    }

    render() {
        return (
            <div className="container row border rounded profile-order-item-style">
                <div className="col-2 border-left"><p style={{padding: "10px"}}>1</p></div>
                <div className="col-6 border-left">
                    <button type="button" className="btn btn-link" data-toggle="modal"
                            data-target={"." + this.state.modalId}
                            onClick={() => this.props.onOrderClick(this.state.order)}>
                        {this.state.order.restaurant.name}
                    </button>
                    {/*<p style={{padding: "10px"}}></p>*/}
                </div>
                <div className="col-4"><p style={{padding: "10px"}}>{this.state.order.status}</p></div>
            </div>
        );
    }
}