import React from 'react';
import {Link} from 'react-router-dom';
import {RESTAURANT_LOGO} from "../../images";

export class RestaurantItem extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="shadow-box justify-content-center m-3">
                <img src={RESTAURANT_LOGO} style={{width: "80px", height: "80px"}}/>
                <div>{this.props.name}</div>
                <Link to={"/menu/" + this.props.id} className="btn btn-add-cart">
                    نمایش منو
                </Link>
            </div>
        );
    }
}