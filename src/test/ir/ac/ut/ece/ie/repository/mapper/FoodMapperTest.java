package ir.ac.ut.ece.ie.repository.mapper;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.repository.TestConnectionPool;
import ir.ac.ut.ece.ie.repository.food.FoodMapper;
import ir.ac.ut.ece.ie.repository.menu.MenuMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FoodMapperTest {
    private FoodEntity test;
    private FoodMapper foodMapper;
    private MenuMapper menuMapper;
    private Connection connection;

    private final String ID = "food1";
    private final String NAME = "kabab";
    private final String IMAGE = "sample_logo";
    private final String DESCRIPTION = "very good food";
    private final Double POPULARITY = 2.2D;
    private final BigDecimal PRICE = new BigDecimal(10000);
    private final MenuEntity MENU = new MenuEntity("menu1", null);

    @Before
    public void setUp() throws Exception {
        foodMapper = new FoodMapper();
        menuMapper = new MenuMapper();
        connection = TestConnectionPool.getConnection();
        menuMapper.save(MENU);
    }

    @Test
    public void testSaveEntity() throws SQLException {
        test = new FoodEntity(ID, NAME, DESCRIPTION, PRICE, POPULARITY, IMAGE, MENU);
        foodMapper.save(test);
        FoodEntity actual = foodMapper.load(ID);
        FoodEntity expected = new FoodEntity(ID, NAME, DESCRIPTION, PRICE, POPULARITY, IMAGE, MENU);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getImage(), actual.getImage());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getPrice().intValue(), actual.getPrice().intValue());
        assertEquals(expected.getPopularity(), actual.getPopularity());
        assertEquals(expected.getMenu().getId(), actual.getMenu().getId());
        foodMapper.delete(ID);
    }

    @Test
    public void testGetEntitiesByProp() throws SQLException {
        FoodEntity food1 = new FoodEntity();
        food1.setId("id1");
        food1.setName("atf");
        food1.setMenu(MENU);
        FoodEntity food2 = new FoodEntity();
        food2.setId("id2");
        food2.setName("atf");
        food2.setMenu(MENU);
        List<FoodEntity> expected = new ArrayList<>(Arrays.asList(food1, food2));
        foodMapper.save(food1);
        foodMapper.save(food2);
        List<FoodEntity> actual = foodMapper.getEntitiesByProp(FoodEntity.COL_NAME, "atf");
        assertEquals(expected.size(), actual.size());
        expected.stream().forEach(item -> assertTrue(expected.contains(item)));
        foodMapper.delete(food1.getId());
        foodMapper.delete(food2.getId());
    }

    @After
    public void tearDown() throws Exception {
        menuMapper.delete(MENU.getId());
        connection.close();
    }
}
