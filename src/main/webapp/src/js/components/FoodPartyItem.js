import React from 'react';
import {RESTAURANT_LOGO} from "../../images";

export class FoodPartyItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div className="shadow-box justify-content-center m-3">
                <div className="row">
                    <img className="col-6" src={RESTAURANT_LOGO} style={{width: "80px", height: "80px"}}/>
                    <div className="col-6">
                        <div>{this.props.FoodPartyItem.food.name}</div>
                        <div>5</div>
                    </div>
                </div>
                <div className="row justify-content-around">
                    <div className="old-price">{this.props.FoodPartyItem.food.price}</div>
                    <div>{this.props.FoodPartyItem.newPrice}</div>
                </div>
                <div className="row justify-content-around">
                    <div style={{fontSize: 10}}>موجودی: {this.props.FoodPartyItem.count}</div>
                    <button type="button" className="btn btn-info btn-sm" style={{fontSize: 10}}
                            onClick={() => this.navigateToRestaurantMenu()}>
                        خرید
                    </button>
                </div>
                <div className="modal-footer">
                    {this.props.FoodPartyItem.restaurant.name}
                </div>
            </div>
        );
    }

}