package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.Exception.LocalizedException;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.manager.jwt.JwtTokenUtil;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.user.UserRepository;

import java.math.BigDecimal;
import java.util.List;

public class UserManager extends BaseManager<UserEntity>{

    public static final String SECRET_KEY = "loghme";
    private static UserManager userManager = new UserManager();

    private JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

    public static UserManager getInstance() {
        return userManager;
    }
    private UserManager() {
        super.loadAll();
//        if(entites.size()==0)
//        {
//            UserEntity user = new UserEntity("user1", "hosein", "ahangari", "09332635009", "test@test",new BigDecimal(1000), "sample password", "sample username", new Location(10, 10));
//            super.saveOrUpdate(user);
//        }
    }

    public void increaseCredit(UserEntity user, int amount) {
        user.setCredit(new BigDecimal(user.getCredit().intValue() + amount));;
        super.saveOrUpdate(user);
    }

    public void decreaseCredit(UserEntity user, int amont){
        user.setCredit(new BigDecimal(user.getCredit().intValue() - amont));
        super.saveOrUpdate(user);
    }

    public void addOrder(OrderEntity orderEntity){

    }

    @Override
    protected BaseRepository getRepository() {
        return UserRepository.getInstance();
    }

    public List<OrderEntity> getAllOrders(String userId) {
        return OrderManager.getInstance().getOrdersByUserId(userId);
    }

    public void resgisterUser(UserEntity user){
        String sha256hex = hashString(user.getPassword());
        user.setPassword(sha256hex);
        super.saveOrUpdate(user);
    }

    public LoginResponseDto loginUser(UserEntity user) throws LocalizedException {
        if(authenticateUser(user))
        {
            return new LoginResponseDto(user, jwtTokenUtil.generateToken(user));
        }
        else {
            throw new LocalizedException(getClass(), "loginUser", "userName or password incorrect");
        }
    }

    private boolean authenticateUser(UserEntity user) {
        String userName = user.getUserName();
        String password = user.getPassword();
        UserEntity loaded = UserRepository.getInstance().loadByUserName(userName);
        String actual = hashString(password);
        return  actual.equals(loaded.getPassword());
    }
}
