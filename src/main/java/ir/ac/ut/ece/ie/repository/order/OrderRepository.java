package ir.ac.ut.ece.ie.repository.order;

import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.OrderEntity;
import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.mapper.ConnectionPool;
import ir.ac.ut.ece.ie.repository.food.FoodRepository;
import ir.ac.ut.ece.ie.repository.dao.OrderItemDao;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ir.ac.ut.ece.ie.repository.dao.OrderItemDao.*;

public class OrderRepository extends BaseRepository<OrderEntity> {
    private static OrderRepository instance = new OrderRepository();

    public static OrderRepository getInstance() {
        return instance;
    }

    private OrderRepository() {
    }

    public List<OrderEntity> getOrdersByUserId(String userId){
        return getEntityMapper().getEntitiesByProp(OrderEntity.COL_USER, userId);
    }

    public List<OrderItem> getOrderItems(String id) {
        List<OrderItem> items = new ArrayList<>();
        for (OrderItemDao dao : this.loadDaoByOrderId(id)) {
            FoodEntity food = FoodRepository.getInstance().load(dao.getFoodId());
            items.add(new OrderItem(food, dao.getCount()));
        }
        return items;
    }

    public OrderItemDao loadDao(String orderId, String foodId) {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(loadManyToManyStatement(new OrderItemDao(orderId, foodId, new BigDecimal(0))))
        ) {
            try {
                ResultSet rs = st.executeQuery();
                if (!rs.next()) {
                    System.out.println("OrderRepository.loadDao: entity is not exist!");
                    return null;
                }
                return convertResultSetToDao(rs);
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.loadDao query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void saveOrderItems(String orderId, List<OrderItem> items) {
        for (OrderItem orderItem : items) {
            OrderItemDao dao = new OrderItemDao(orderId, orderItem.getFood().getId(), orderItem.getCount());
            if (this.loadDao(dao.getOrderId(), dao.getFoodId()) != null) {
                System.out.println("OrderRepository.saveDao: dao is exist!");
                return;
            }
            super.saveDao(dao);
        }
    }

    public void deleteDao(OrderItemDao dao) {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(deleteManyToManyStatement(dao))
        ) {
            try {
                st.executeUpdate();
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.deleteDao query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<OrderItemDao> loadDaoByOrderId(String orderId) {
        List<OrderItemDao> result = new ArrayList<>();
        OrderItemDao dao = new OrderItemDao(orderId, ALL, new BigDecimal(0));
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement st = con.prepareStatement(loadManyToManyStatement(dao))
        ) {
            ResultSet resultSet;
            try {
                resultSet = st.executeQuery();
                while (resultSet.next())
                    result.add(convertResultSetToDao(resultSet));
                return result;
            } catch (SQLException ex) {
                System.out.println("error in OrderRepository.loadDaoById query.");
                System.out.println(st);
                throw ex;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected OrderItemDao convertResultSetToDao(ResultSet resultSet) {
        try {
            String orderId = resultSet.getString(M2M_COL_ORDER);
            String foodId = resultSet.getString(M2M_COL_FOOD);
            BigDecimal count = resultSet.getBigDecimal(M2M_COL_COUNT);
            return new OrderItemDao(orderId, foodId, count);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected Mapper<OrderEntity, String> getEntityMapper() {
        return new OrderMapper();
    }

//    private <T> List<T> getManyToManyResultObject(ResultSet resultSet, Class<T> orderItemClass) {
//
//    }
}
