package ir.ac.ut.ece.ie.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantEntity extends BaseEntity<String> {
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_LOCATION = "location";
    public static final String COL_LOGO = "logo";
    public static final String COL_MENU = "menu_id";

    protected String id;
    protected String name;
    protected Location location;
    protected String logo;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MenuEntity menu;

    @JsonProperty("menu")
    private void unpackListOfFood(List<FoodEntity> foods) {
        MenuEntity menu = new MenuEntity(foods);
        this.menu = menu;
    }

    public RestaurantEntity(String id) {
        this.id = id;
    }


    public boolean restaurantHasFood(String foodName) {
        return menu.menuHasFood(foodName);
    }

    public void addFood(FoodEntity food) {
        menu.addFood(food);
    }

}
