package ir.ac.ut.ece.ie.service.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuDto {
    @JsonUnwrapped
    MenuEntity menuEntity;
    String restaurantName;
}
