package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.entities.*;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import ir.ac.ut.ece.ie.repository.food.FoodRepository;
import ir.ac.ut.ece.ie.repository.menu.MenuRepository;
import ir.ac.ut.ece.ie.repository.order.OrderRepository;
import ir.ac.ut.ece.ie.repository.user.UserRepository;

import java.math.BigDecimal;

public abstract class TestUtil {
    public MenuEntity saveMenu(String id){
        MenuEntity menuEntity = new MenuEntity(id);
        MenuRepository.getInstance().saveOrUpdate(menuEntity);
        return menuEntity;
    }

    public FoodEntity saveFood(String id, MenuEntity menu){
        FoodEntity food = new FoodEntity(id, "kabab", "desc", new BigDecimal(10), 2.2D, "test", menu);
        FoodRepository.getInstance().saveOrUpdate(food);
        return food;
    }

    public OrderEntity saveOrder(String id, UserEntity user){
        OrderEntity orderEntity = new OrderEntity(id, user, OrderStatus.LOOKING_FOR_DELIVERY);
        OrderRepository.getInstance().saveOrUpdate(orderEntity);
        return orderEntity;
    }

    public UserEntity saveUser(String id){
        UserEntity userEntity = new UserEntity(id, "hosein", "ahangari", "09332635009", "test@test.com", new BigDecimal(10d), "sample password", "sample username", new Location(10 , 10));
        UserRepository.getInstance().saveOrUpdate(userEntity);
        return userEntity;
    }
}
