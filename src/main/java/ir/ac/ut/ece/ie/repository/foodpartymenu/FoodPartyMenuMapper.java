package ir.ac.ut.ece.ie.repository.foodpartymenu;

import ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity.COL_ID;
import static ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity.COL_RESTAURANT;

public class FoodPartyMenuMapper extends Mapper<FoodPartyMenuEntity, String> {
    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        columns.put(COL_RESTAURANT, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, FoodPartyMenuEntity foodPartyMenuEntity) {
        switch (column){
            case COL_ID:
                return foodPartyMenuEntity.getId();
            case COL_RESTAURANT:
                return foodPartyMenuEntity.getRestaurant().getId();
            default:
                return null;
        }
    }

    @Override
    protected FoodPartyMenuEntity convertResultSetToObject(ResultSet rs) throws SQLException {
        FoodPartyMenuEntity foodPartyMenuEntity = new FoodPartyMenuEntity();
        foodPartyMenuEntity.setId(rs.getString(COL_ID));
        foodPartyMenuEntity.setRestaurant(new RestaurantEntity(rs.getString(COL_RESTAURANT)));
        return foodPartyMenuEntity;
    }

    @Override
    public String getDefaultTable() {
        return Constants.FOODPARTY_MENU_TABLE;
    }
}
