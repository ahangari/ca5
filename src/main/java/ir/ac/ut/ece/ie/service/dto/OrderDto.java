package ir.ac.ut.ece.ie.service.dto;

import ir.ac.ut.ece.ie.entities.model.OrderItem;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.entities.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    RestaurantEntity restaurant;
    List<OrderItem> items;
    OrderStatus status;
}
