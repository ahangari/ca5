package ir.ac.ut.ece.ie.repository.restaurant;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.Constants;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static ir.ac.ut.ece.ie.entities.RestaurantEntity.*;
public class RestaurantMapper extends Mapper<RestaurantEntity, String> {


    @Override
    protected Map<String, Class<?>> getColumns() {
        Map<String, Class<?>> columns = new HashMap<>();
        columns.put(COL_ID, String.class);
        columns.put(COL_NAME, String.class);
        columns.put(COL_LOCATION, String.class);
        columns.put(COL_LOGO, String.class);
        columns.put(COL_MENU, String.class);
        return columns;
    }

    @Override
    protected Object getColmunValue(String column, RestaurantEntity restaurantEntity) {
        switch (column){
            case COL_ID:
                return restaurantEntity.getId();
            case COL_NAME:
                return restaurantEntity.getName();
            case COL_LOCATION:
                return restaurantEntity.getLocation();
            case COL_LOGO:
                return restaurantEntity.getLogo();
            case COL_MENU:
                return restaurantEntity.getMenu().getId();
            default:
                return null;
        }
    }

    @Override
    protected RestaurantEntity convertResultSetToObject(ResultSet rs) throws SQLException{
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(rs.getString(RestaurantEntity.COL_ID));
        restaurantEntity.setName(rs.getString(RestaurantEntity.COL_NAME));
        restaurantEntity.setLogo(rs.getString(RestaurantEntity.COL_LOGO));
        try {
            restaurantEntity.setLocation(new ObjectMapper().readValue(rs.getString(RestaurantEntity.COL_LOCATION), Location.class));
        } catch (IOException e) {
            e.printStackTrace();
            restaurantEntity.setLocation(null);
        }
        restaurantEntity.setMenu(new MenuEntity(rs.getString(RestaurantEntity.COL_MENU), null));
        return restaurantEntity;
    }

    @Override
    public String getDefaultTable() {
        return Constants.RESTAURANT_TABLE;
    }
}
