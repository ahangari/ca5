package ir.ac.ut.ece.ie.service;

import ir.ac.ut.ece.ie.Exception.RestaurantNotFoundException;
import ir.ac.ut.ece.ie.entities.CartEntity;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.manager.CartManager;
import ir.ac.ut.ece.ie.manager.FoodPartyMenuManager;
import ir.ac.ut.ece.ie.manager.MenuManager;
import ir.ac.ut.ece.ie.manager.RestaurantManager;
import ir.ac.ut.ece.ie.service.dto.FoodPartyDto;
import ir.ac.ut.ece.ie.service.dto.MenuDto;
import ir.ac.ut.ece.ie.service.dto.RestaurantListDto;
import ir.ac.ut.ece.ie.service.util.ActionResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RestaurantService {


    @RequestMapping(value = "/getRestaurants", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public RestaurantListDto getRestaurants() {
        List<RestaurantEntity> restaurants = new ArrayList<>();
        RestaurantManager.getInstance().loadAll().forEach((key, value)-> restaurants.add(value));
        RestaurantListDto dto = new RestaurantListDto(restaurants);
        return dto;
    }

    @RequestMapping(value = "/getMenu/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MenuDto getMenu(@PathVariable("id")String id) throws RestaurantNotFoundException {
        RestaurantEntity restaurant = RestaurantManager.getInstance().load(id);
        MenuEntity menu = MenuManager.getInstance().load(restaurant.getMenu().getId());
        return new MenuDto(menu, restaurant.getName());
    }

    @PutMapping(value = "/cart")
    public ActionResult finalizeCart(@RequestBody CartEntity cart) {
        return CartManager.getInstance().finalize(cart);
    }

    @GetMapping(value = "/foodparty")
    public FoodPartyDto getFoodParty(){
        FoodPartyDto dto = new FoodPartyDto();
        dto.setFoodPartyItems(FoodPartyMenuManager.getInstance().loadLiveFoodPartyMenu());
        return dto;

    }

//    @RequestMapping(value = "/", method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public BookingResult bookTheFlight(
//            @RequestParam(value = "destination") String destination,
//            @RequestParam(value = "numberOfTickets") int numberOfTickets,
//            @RequestParam(value = "firstName") String firstName,
//            @RequestParam(value = "lastName") String lastName) {
//        FlightManager.getInstance().bookFlight(destination, numberOfTickets);
//        BookingResult result = new BookingResult();
//        result.setSuccessful(true);
//        return result;
//    }
}
