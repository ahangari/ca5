package ir.ac.ut.ece.ie.manager;

import ir.ac.ut.ece.ie.Utils;
import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.restaurant.RestaurantRepository;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RestaurantManager extends BaseManager<RestaurantEntity> {

    private static RestaurantManager instance = new RestaurantManager();

    public static RestaurantManager getInstance() {
        return instance;
    }

    public void updateRestaurants(){
        RestaurantEntity[] restaurantsList = new RestaurantEntity[0];
        try {
            restaurantsList = Utils.requestJson("http://138.197.181.131:8080/restaurants", RestaurantEntity[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (RestaurantEntity restaurant : restaurantsList) {
            MenuManager.getInstance().saveOrUpdate(restaurant.getMenu());
            for (FoodEntity food : restaurant.getMenu().getFoods()) {
                food.setMenu(restaurant.getMenu());
                FoodManager.getInstance().saveOrUpdate(food);
            }
            this.saveOrUpdate(restaurant);
        }
    }
    private RestaurantManager(){
        super.loadAll();
//        updateRestaurants();
    }


    public void addRestaurant(RestaurantEntity restaurantEntity) {
        for (RestaurantEntity restaurant : this.loadAll().values())
            if (restaurant.getName() == restaurantEntity.getName())
                return;
//        restaurants.add(restaurantEntity);
    }

//    public boolean restaurantExists(String restaurantName) {
//        for (int i = 0; i < restaurants.size(); i++)
//            if (restaurants.get(i).getName().equals(restaurantName))
//                return true;
//        return false;
//    }

//    public void addFood(FoodEntity food) {
//        for (RestaurantEntity restaurant : restaurants)
//            if (restaurant == FoodManager.getInstance().getRestaurant(food))
//                restaurant.addFood(food);
//    }

//    public void getRestaurantNames() {
//        for (RestaurantEntity restaurant : restaurants)
//            System.out.println(restaurant.getName());
//    }

//    public RestaurantEntity getRestaurantByName(String restaurantName) {
//        for (RestaurantEntity restaurant : restaurants)
//            if (restaurant.getName().equals(restaurantName))
//                return restaurant;
//        return null;
//    }

//    public RestaurantEntity getRestaurantById(String id) throws RestaurantNotFoundException {
//        for (RestaurantEntity restaurant : restaurants)
//            if (restaurant.getId().equals(id))
//                return restaurant;
//
//        RestaurantEntity entity = this.load(id);
//
//        if(entity == null)
//            throw new RestaurantNotFoundException("no restaurant found");
//
//        return entity;
//
//    }


    private double calculateDistanceFromUser(Location userLocation, RestaurantEntity restaurant) {
        return Math.sqrt(Math.pow(restaurant.getLocation().getX() - userLocation.getX(), 2D) +
                Math.pow(restaurant.getLocation().getY() - userLocation.getY(), 2D));
    }

    public List<RestaurantEntity> filterWithDistance(RestaurantEntity[] restaurantsList, Location location, double validDistance) {
        List<RestaurantEntity> result = new ArrayList<>();
        for (RestaurantEntity restaurant : restaurantsList) {
            if (calculateDistanceFromUser(location, restaurant) <= validDistance)
                result.add(restaurant);
        }
        return result;
    }


    public RestaurantEntity getRestaurantByMenu(MenuEntity menuEntity) {
        return RestaurantRepository.getInstance().getRestaurantByMenu(menuEntity.getId());
    }

    @Override
    protected BaseRepository getRepository() {
        return RestaurantRepository.getInstance();
    }

//    public RestaurantDisplayDTO checkAndGetRestaurant(String id, Location userLocation, double valid_distance){
//        RestaurantDisplayDTO returnVal = new RestaurantDisplayDTO(null, Status.NOT_EXIST);
//
//        for (RestaurantEntity Restaurant: restaurants) {
//            if (Restaurant.getId().equals(id)) {
//                if (valid_distance > calculateDistanceFromUser(userLocation, Restaurant)) {
//                    returnVal.setStatus(Status.LONG_DISTANCE);
//                    return returnVal;
//                }
//                returnVal.setRestaurant(Restaurant);
//                returnVal.setStatus(Status.OK);
//                return returnVal;
//            }
//        }
//        return returnVal;
//    }
}
