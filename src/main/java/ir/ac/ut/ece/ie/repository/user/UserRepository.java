package ir.ac.ut.ece.ie.repository.user;

import ir.ac.ut.ece.ie.entities.UserEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;
import static ir.ac.ut.ece.ie.entities.UserEntity.*;

public class UserRepository extends BaseRepository<UserEntity> {
    private static UserRepository instance = new UserRepository();
    public static UserRepository getInstance(){
        return instance;
    }
    private UserRepository(){}

    @Override
    protected Mapper<UserEntity, String> getEntityMapper() {
        return new UserMapper();
    }

    public UserEntity loadByUserName(String userName) {
        return getEntityMapper().getEntitiesByProp(COL_USERNAME, userName).get(0);
    }
}
