package ir.ac.ut.ece.ie.repository.foodpartymenu;

import ir.ac.ut.ece.ie.Exception.LocalizedException;
import ir.ac.ut.ece.ie.entities.FoodEntity;
import ir.ac.ut.ece.ie.entities.FoodPartyMenuEntity;
import ir.ac.ut.ece.ie.entities.model.FoodPartyItem;
import ir.ac.ut.ece.ie.manager.FoodManager;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.dao.FoodPartyItemDao;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

import java.util.ArrayList;
import java.util.List;

public class FoodPartyMenuRepository extends BaseRepository<FoodPartyMenuEntity> {
    private static FoodPartyMenuRepository instance = new FoodPartyMenuRepository();
    public static FoodPartyMenuRepository getInstance(){
        return instance;
    }
    private FoodPartyMenuRepository(){}
    @Override
    protected Mapper<FoodPartyMenuEntity, String> getEntityMapper() {
        return new FoodPartyMenuMapper();
    }

    public List<FoodPartyItemDao> loadFoodPartyItems(String foodPartyMenuId){
        FoodPartyItemDao daoForSearch = new FoodPartyItemDao(foodPartyMenuId, null, null, null);
        List<FoodPartyItemDao> result = new ArrayList<>();
        this.loadDaoSingleParam(daoForSearch).stream().forEach(dao -> result.add((FoodPartyItemDao)dao));
        return result;
    }

    public void saveFoodPartyItems(String FoodPartyMenuId, List<FoodPartyItem> items) throws LocalizedException{
        for (FoodPartyItem foodPartyItem : items) {
            FoodEntity food = FoodManager.getInstance().loadByName(foodPartyItem.getFood().getName());
            if(food==null)
                throw new LocalizedException(FoodPartyMenuRepository.class, "saveFooPartyItems","food not exist in menu!");

            FoodPartyItemDao dao = new FoodPartyItemDao(FoodPartyMenuId, food.getId(), foodPartyItem.getNewPrice(), foodPartyItem.getCount());
            if (this.loadDaoDoubleParam(dao) != null) {
                System.out.println("OrderRepository.saveDao: dao is exist!");
                return;
            }
            super.saveDao(dao);
        }
    }
}
