package ir.ac.ut.ece.ie.repository.restaurant;

import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.BaseRepository;
import ir.ac.ut.ece.ie.repository.mapper.Mapper;

public class RestaurantRepository extends BaseRepository<RestaurantEntity> {
    private static RestaurantRepository instance = new RestaurantRepository();

    private RestaurantRepository() {

    }


    public static RestaurantRepository getInstance() {
        return instance;
    }

    public RestaurantEntity getRestaurantByMenu(String menuId) {
        RestaurantMapper restaurantMapper = new RestaurantMapper();
        return restaurantMapper.getEntitiesByProp(RestaurantEntity.COL_MENU, menuId).get(0);
    }

    @Override
    protected Mapper<RestaurantEntity, String> getEntityMapper() {
        return new RestaurantMapper();
    }
}
