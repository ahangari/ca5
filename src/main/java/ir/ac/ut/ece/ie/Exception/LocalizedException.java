package ir.ac.ut.ece.ie.Exception;

import lombok.Getter;

@Getter
public class LocalizedException extends Exception {
    String msg;
    public LocalizedException(Class<?> location, String method, String msg){
        this.msg = location.toString() + "." + method + ": " + msg;
    }
}
