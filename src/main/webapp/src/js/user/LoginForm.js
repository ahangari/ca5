import React from 'react';
import {Link, Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import {loginUser} from "../reducer";

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                userName: null,
                password: null
            },
            redirectToHome: this.props.auth.redirect
        }
    }
    renderRedirect = (path) => {
        return <Redirect to={path} />
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(this.props.auth.redirect)
            this.setState({redirectToHome: true});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loginUser(this.state.user);
    }

    handleChangeUserName(event) {
        let user = {...this.state.user, userName: event.target.value}
        this.setState({user});
    }

    handleChangePassword(event) {
        let user = {...this.state.user, password: event.target.value}
        this.setState({user});
    }

    render() {
        return (
            this.state.redirectToHome?
                this.renderRedirect('/dashboard'):
            <div className="form_container">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <h2>صفحه ورود</h2>
                    </div>
                    <div className="container">
                        <label className="credential_labels"><b>نام کاربری</b></label>
                        <input type="text" value={this.state.user.userName}
                               onChange={this.handleChangeUserName.bind(this)}
                               placeholder="نام کاربری خود را وارد کنید" required/>
                        <b></b>
                        <label className="credential_labels"><b>رمز عبور</b></label>
                        <input type="password" value={this.state.user.password}
                               onChange={this.handleChangePassword.bind(this)}
                               placeholder="رمز عبور خود را وارد کنید" required/>
                        <b></b>
                        <button type="submit" className="submit-btn-style">
                            ورود
                        </button>
                        <label>
                            <input type="checkbox" checked="checked" name="remember"/> مرا به خاطر داشته باش
                        </label>
                        <br/>
                        <a href="#">رمز عبور خود را از یاد برده اید؟</a>
                    </div>
                    <div className="container" style={{backgroundColor: "#f1f1f1"}}>
                        <button type="button" className="down_button">ورود با استفاده از Google Authentication</button>
                    </div>
                    <div className="container" style={{backgroundColor: "#f1f1f1"}}>
                        <Link to="/signup" className="down_button">هنوز عضو نیستم</Link>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    }
};

export default connect(mapStateToProps, {
    loginUser
})(LoginForm)