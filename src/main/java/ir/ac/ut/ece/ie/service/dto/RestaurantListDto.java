package ir.ac.ut.ece.ie.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class RestaurantListDto {
    @JsonIgnoreProperties(value = {"menu"})
    private List<RestaurantEntity> restaurants;
}
