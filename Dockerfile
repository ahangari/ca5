FROM tomcat:9.0.33-jdk11-openjdk

ADD ./target/IEREACT.war /usr/local/tomcat/webapps/

LABEL mainpotainer="hoseinahangari328@gmail.com"

EXPOSE 8080

CMD ["catalina.sh", "run"]
