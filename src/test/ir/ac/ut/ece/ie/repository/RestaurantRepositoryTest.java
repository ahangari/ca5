package ir.ac.ut.ece.ie.repository;

import ir.ac.ut.ece.ie.entities.Location;
import ir.ac.ut.ece.ie.entities.MenuEntity;
import ir.ac.ut.ece.ie.entities.RestaurantEntity;
import ir.ac.ut.ece.ie.repository.menu.MenuMapper;
import ir.ac.ut.ece.ie.repository.restaurant.RestaurantMapper;
import ir.ac.ut.ece.ie.repository.restaurant.RestaurantRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class RestaurantRepositoryTest {

    private final String ID = "restaurant1";
    private final String NAME = "atf";
    private final String LOGO = "sample_logo";
    private final Location LOCATION = new Location(10, 20);
    private final MenuEntity MENU = new MenuEntity("menu1", null);

    private Connection connection;
    private MenuMapper menuMapper;
    private RestaurantMapper restaurantMapper;

    @Before
    public void setUp() throws Exception {
        connection = TestConnectionPool.getConnection();
        menuMapper = new MenuMapper();
        menuMapper.save(MENU);

        restaurantMapper = new RestaurantMapper();
        RestaurantEntity restaurant = new RestaurantEntity(ID, NAME, LOCATION, LOGO, MENU);
        restaurantMapper.save(restaurant);
    }

    @Test
    public void getRestaurantByMenu() throws SQLException {
        RestaurantEntity actual = RestaurantRepository.getInstance().getRestaurantByMenu(MENU.getId());
        assertEquals(MENU.getId(), actual.getMenu().getId());
    }

    @After
    public void tearDown() throws Exception {
        restaurantMapper.delete(ID);
        menuMapper.delete(MENU.getId());
        connection.close();
    }

}