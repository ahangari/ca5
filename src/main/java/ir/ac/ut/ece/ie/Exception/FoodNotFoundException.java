package ir.ac.ut.ece.ie.Exception;

public class FoodNotFoundException extends Exception {
    public FoodNotFoundException(String message) {
        super(message);
    }

    public FoodNotFoundException() {}
}
